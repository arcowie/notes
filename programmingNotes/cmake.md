* CMake Guide: [[https://cmake.org/training/][Documentation]]

* ALL CMakeLists.txt require as a first line
** cmake_minimum_required(<version number>)

* Basic layout
#+begin_src cmake
  cmake_minimum_required(3.22.1)

  #give project name
  project(test VERSION 1.0)

  #for version
  configure_file(testConfig.h.in testConfig.h)

  #Write .h with version number to the directory that builds
  target_include_directories(${PROJECT_NAME} PUBLIC ${PROJECT_BINARY_DIR})

  #give output target and dependencies
  add_executable(${PROJECT_NAME} someFile.c)

  #If libraries other files that need to be made will call cmake in that dir
  add_subdirectory(some/other/dir)

  #if lib not in standard dir note can specify relative to source directory with var ${CMAKE_SOURCE_DIR}
  target_link_directories(${PROJECT_NAME}
    PUBLIC /path/to/lib
    PUBLIC /path/to/other/lib
    )
  #if include not in standard directories
  target_include_directories(${PROJECT_NAME}
    PUBLIC /path/to/include
    PUBLIC /path/to/other/include
  )
  #for libraries to link against
  target_link_libraries(${PROJECT_NAME} someLibName, someOtherLibName)
#+end_src
** Need to have a file anmed testConfig.h.in
#+begin_src c
  #define test_VERSION_MAJOR @test_VERSION_MAJOR@
  #define test_VERSION_MINOR @test_VERSION_MINOR@
#+end_src
*** need to #include <testConfig.h>

* Layout for creating a static library
#+begin_src cmake
  cmake_minimum_required(3.22.1)

  #give project name
  project(testLib)

  #Use add_library in place of add_execultable
  add_library(testLib myLib.c)

  #have to set target properties so it knows about header files
  set_target_properties(testLib PROPERTIES PUBLIC_HEADER myLib.h)

  #install will put lib into standard directory and also remember headers
  install(TARGETS testLib LIBRARY DESTINATION lib PUBLIC_HEADER DESTINATION include)
#+end_src

* External libs
** Git submodules
*** To add libraries from git
*** First have to be in a repo/directory with git
*** command
#+begin_src shell
  git submodule add https://url/to/repo dir/to/clone/into
#+end_src
*** Need to Stage changes before using!
*** .gitmodules contains submodule information
*** If there is a cmake in that directory it will call it.

*** to CMakeLists.txt add
#+begin_src cmake
#
## get glfw from github
find_package(Git QUIET)
if(GIT_FOUND AND EXISTS "${PROJECT_SOURCE_DIR}/.git")
# Update submodules as needed
    option(GIT_SUBMODULE "Check submodules during build" ON)
    if(GIT_SUBMODULE)
        message(STATUS "Submodule update")
        execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
                        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                        RESULT_VARIABLE GIT_SUBMOD_RESULT)
        if(NOT GIT_SUBMOD_RESULT EQUAL "0")
            message(FATAL_ERROR "git submodule update --init failed with ${GIT_SUBMOD_RESULT}, please checkout submodules")
        endif()
    endif()
endif()

#           CHECK ALL THE SUBMODULES
if(NOT EXISTS "${PROJECT_SOURCE_DIR}/external/glfw/CMakeLists.txt")
    message(FATAL_ERROR "The glfw submodules was not downloaded! GIT_SUBMODULE was turned off or failed. Please update submodules and try again.")
endif()

  add_subdirectory(dir/to/cloned/module)
#+end_src
** find_library
*** Put it anywhere before you link
*** Basic syntax
#+begin_src cmake
  #if you don't use required have to use if statment
  find_library(<Variable name> <Name of Lib> NAMES <Any Other names of lib this is optional> PATHS "path/to/look/for/libs DOC "Doc string" REQUIRED[OPTIONAL] )
#+end_src
*** With an if statement
#+begin_src cmake
  find_library(myLib alsCoolLib DOC "The best Lib ever")
  if (${myLib} STREQUAL myLIB-NOTFOUND)
    message(FATAL_ERROR "libalsCoolLib not found")
  else()
    message(STATUS "libalsCoolLib found as ${myLib}")
    list(APPEND EXTRA_INCLUDE_DIRS path/to/includes)
    ##could do: list(APPEND EXTRA_LINKS ${myLIB})
   
  target_include_directories(${PROJECT_NAME} /path/to/reg/includes ${EXTRA_INCLUDE_DIRS}) 
  target_link_libraries($(PROJECT_NAME) someLib ${myLib} #{EXTRA_LINKS})
#+end_src
** find_package
*** Needs a configure and cmake

* Options
** Basic syntax
#+begin_src cmake
   option(<Variable name> "Text about what it is" <Default value ON/OFF>)
#+end_src
** Example
#+begin_src cmake
    option(USE_OPT_LIB "Text about what it is" ON)
              #Example if statement in this instance to add an optional library
              if(USE_OPT_LIB)
                #statments
                add_subdirectory(dir/to/add)

  	      list(APPEND EXTRA_LIBS_DIRS "dir/extra/Lib")
  	      list(APPEND EXTRA_INCLUDES_DIRS "dir/to/include")
  	      list(APPEND EXTRA_LINKS nameOfLIB)
  	      
                endif()

  target_include_directories(${PROJECT_NAME}
     PUBLIC extra/include/dirs
     ${EXTRA_INCLUDES_DIRS}
     )

   target_link_directories(${PROJECT_NAME}
      	   PUBLIC extra/lib/dirs
    	   ${EXTRA_LIBS_DIRS}
  	   )
   
   target_link_libraries(${PROJECT_NAME} libOne libTwo ${EXTRA_LINKS})
   
        ## In the file specified in configure_file also add ifdef to code
        ## This is for using optional libraries
        #cmakedefine USE_ADDER

  	#ifdef USE_SOME_LIB in file like main.c
             #include <header.h>
        #endif
#+end_src

* Installing
** For Libs
#+begin_src cmake
  install(TARGETS libName DESTINATION lib)
  install(FILES libname.h DESTINATION include)
#+end_src
** Main cmake
#+begin_src cmake
  install(TARGETS ${PROJECT_NAME} DESTINATION bin)
  #config file that has version info
  install(FILES "${PROJECT_BINARY_DIR}/configFile.h" DESTINATION include)
#+end_src
** CPack
*** Makes an install and uninstall script and packages files up.
*** Needs to be in top level cmake
#+begin_src cmake
  include(InstallRequiredSystemLibraries)
  #Optional but nice
  set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/License.txt")
  set(CPACK_PACKAGE_VERSION_MAJOR "${Name_VERSION_MAJOR}")
  set(CPACK_PACKAGE_VERSION_MINOR "${Name_VERSION_MINOR}")
  include(CPack)
#+end_src
*** run cpack in build dir
*** creates three files
**** /.sh/ which installs a /.tar.gz/ and a /tar.z/

* Variables
** set()
*** basic syntax
#+begin_src cmake
set(<Variable name> "Some Value")
set(<Variable name> "Some value ${Another Variable name}" )
#+end_src
*** List syntax for set()
#+begin_src cmake
set(<Var name> "a" "b" "c")
set(<Var name> ${Some Var} "d")
#+end_src
** list()
#+begin_src cmake
list(APPEND <Var name> "a" "b" "c")
#+end_src
