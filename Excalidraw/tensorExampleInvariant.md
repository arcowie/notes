---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠== You can decompress Drawing data with the command palette: 'Decompress current Excalidraw file'. For more info check in plugin settings under 'Saving'


# Excalidraw Data
## Text Elements
The Scientist's
height is invariant ^YSIA6SvH

His distance to the 
door is invariant ^Svbs78rl

Coordinate
System A ^GHgBjYMb

Under Coordinate System A
His distance from the door is ^Pig4dlt0

2 ^w4jubXwM

+ ^xXAaKj1y

2.5 ^yGPPNKJk

Coordinate
System B ^jYHJW4Eh

Under Coordinate System B
His distance from the door is 4.5 ^fqXx6u4R

+ ^EyCoRw4Y

0 ^io9QqkzE

However, the components change under different
Coordinate Systems ^MAWZkd1O

## Embedded Files
29777d2a7c80ff59620c2e9a6781b31079fa109e: [[download_1.jpg]]

47f7cf2f6562f1b68bb6aad8e7e879d7c274a186: https://thumbs.dreamstime.com/b/lab-geek-man-24969692.jpg

%%
## Drawing
```compressed-json
N4KAkARALgngDgUwgLgAQQQDwMYEMA2AlgCYBOuA7hADTgQBuCpAzoQPYB2KqATLZMzYBXUtiRoIACyhQ4zZAHoFAc0JRJQgEYA6bGwC2CgF7N6hbEcK4OCtptbErHALRY8RMpWdx8Q1TdIEfARcZgRmBShcZQUebQA2bQB2GjoghH0EDihmbgBtcDBQMBKIEm4INgBVAEEhABl8ADNJVJLIWEQKwn1opH5SzG5nAGYADiTtAEYeeKmAFkWABgBO

KaSVpIHIGGGRqe0RgFYxpZ55+PnTkaWjo+2IChJ1bh4eSamFlZ41le+piYPSQIQjKaTcG5xMZTP63I4rMYrE6zB7WZTBbhLB7MKCkNgAawQAGE2Pg2KQKgBiJY02ltUqaXDYfHKPFCDjEElkikSXHWZhwXCBbL0yBNQj4fAAZVgGIkkiZGkCoogOLxhIA6s9WmgptjcQSEDKYHL0IIPCq2WCOOFcrqHmxBdg1LtdTSHmyOTbmHbUBwhJLsQgEMRu

FMRscVg9GCx2Fxdedo0xWJwAHKcMSvBbxeIrJYLPiFSBCODEXBQENhpL7FbzAFLJJHRsPQjMAAi6QrobQTQIYQerOEcAAksRfXkALoPTTCDkAUWCmWy46LRVXkHKEkwknoNRqK3qpGcNQ1UA4AA0pXOAJrX5QIYei0qdcToIV4qirgC+2zX7VKm7oHOTQAOKphQcBCE0tRwAAYgAKviSwwPobZSgA+gAjk+HTwK+EDvmwn7/j+q7FP+ZTdug8Eas

Q8FwAA0soABCqZjAAsrB6H6AAUsOADyABWzgsjh0B4RUhHEe0X5FlORYQEQHD4tw/qBgpZLMpWPZ9ggDzisEo4VPMSRNEk2BNDwTTxEc8RWVMmjxGMmiObguDEGMCBJAgEwrMQ5nvPMuAAvEKrMO4r4FP+eqrlMckthyWCSUsKpNOQmRGWgan4Pq5ZCL6ECIByhAcMoKrYHicCqQG+CFKRJTkRuVEQDUABK8HtTwzEAAp+JhYwavQHD1FUSzMMxV

UPC+3S9PeKpDGgzhTEsYzaG8RwrTwIybBcqzzA8rqoM4PAzNoTbzEs4xbWMRw8Ec8yFu0jzaq8d1AiCYJQK99wKWippYgpaqGly5IVHyHACkKWRfdOTIsp6nKkqDvLkBDgrCjDCkGdKsr4ea5T6uqCBasQLxoI9pRA4SxqmqqpIEwpVqSN6voxU9jpMi6Ybugpg4lqO47yU9aW4BlVHZQlxBJRIwWWrOxAs9V6lPWE2m8CtiIjI2ZxJrGnBZrrKY

cOmHCZuTN05oitYtu2nZq72+D9gpM7ssQC4ZNDK7/o1AHNTRdGMSxbGcdxfFCSJZW/rhXQy6QH4QN+v4+01FT1AAEqesEUCsc5mJgc7wSM54AIqCfxFDXpoYnTbH8eJ2R66UWD8wrFUKwwCMDHMAAWjw3WSFMRxNN1PCYDa1cSbXREJyRSeN4BECwT354aphkjEHA3cwH58RSt3KwMYQzjsRPMdvnH0/fvFCklmWXZVjWdaIkcEZRhpJUqVlNUPJ

phJUQ7TsVZRCgPlCoRVHClVShKB8zUfhJHgcQHguBzKnCaE0eEdkljYB4AgFYuB4hJGhJofYDYVi9mWisfogMIr5Fir+OK7QhalAqo6JWtUSj1T/L7ComEOAjEEksAAamnBA8R6CaH4vEeo3clj6CECsLiKoa7oCUlQp6C0jojAIckJIuY7I8FOO8EYiwDphgOPEM4N0Vo5neMtLYCknikx1KgI42gTJJC2uMMYBCATNgUsCUE4JyZa2mPEbaqw1

gnVWOMVEpV/qE2BkjHk6BKRNBWtgGkKpGTMj5hyEGyToCo0hhjKBkoaZ43pqGBJmoXrk2qUaXGFR8ZVMZsIa0towwOidFzN0AMnp8xHGOfITCxTpRgWwyW0s3wjDlq7RWaAfbiTPjwIsnCBDBiooPI4Mx5gjFmPEQ2cZuYjEOWmDMr51gTFsqsMYNsOzBHvjpR2elnby3dkuHIwyHi33LGrdYj9lpnC0W/J6SlP5+m/hpNgWl/66QeHANgJVPloC

iu0VF7Q+n/iWKuEZYB0UlGcK49xnixjeKSL4n6/4wBbQ+GEvMfwZgUPGDi6+oLQhQBJPofQaguzdURSKL+ytKZRFIFAZiJUIFlUFTlBSWRiDiuKpA6VuVRU1AvhQYE7kJmyo5Gqj8mrxY1TqgMRqTcJA1EIMxJo2BBI8HYjUIe8FrzOFwOhRBIE05TCUZPFRJU1GDD2E5Q45xczrA8bta2ClDojG0CsYxuiaTzHhCcWyILSiOLJqgINFwX57MujM

SlpQAmfVeNMPMpKkhJs+OSq5sT0SvkxQIA0hJ8lUjSWMDJKVYY5IRq2lG/J0bQ1KTjE0FSLT1JJpmimTaiblKaZU2Z7SfSdIUhzZ0sBuaNogAMgWXysZjMyhCoVG5EoaIIkcWZXoOkLNXEs18KyZJBj+WsAECxtanPjKgSNT0YxGxNmbVAMwK2hrGNOsotsHn2zha8127zPZ7qej8x5gHqwwgun8BEFif4f21aC6Ff9uAAJeU9BFSKvZovXI2ko2

L/y4vxWAbNNkIw8HzW9dccQYQa0rds9YFKWWMJ/uyzl3KZAhj5Ui3DwqhRiolSVKVR6ZVPTlQqyVkmm3Sb1URA1amMC6vVdp6VxrCimoXteKUw4ai73oGnb1Z9oBYExuo4YywEjnEut4lYdlERLAOVG4Yy04h7OMa/GlTkHgZucSx1aGGPEvzrBsJytz/EfSCS4rdf0G31L7Sk9tnaslw1yYjbkYMimDpFPpCUI7abNLCs24mtTeD1LnRIGrHo2n

M2vYBrpnMN29IHGyQZgt9IHsNcesop7JIrEvQrTrEtAYbIhE5H4XnfM/uTEc8mK0P3/ouXcU4L87pbtbPchAyGiPTjeYueDKKRkQCQ381DtYFi3HDGmyAYKdO/yg88qaDmKjwWBKgKUzpoatigAAcmYAAHQ4MW6QqBWwI44PQIUThHOlHIBQeCf2JAA4QEDkH2QweQ5h3DqACPmBI5R6QNHqVOBQClIQIw96t1NHp7BUWEpDpJaehWTAUAahEGUJ

+xSCAmjo8gDGKA5gCCC9BCLqArCHh6GyLgP1pBD1zfZjT/wBBsf8/+4D4HhBQc4hJ7DlL5PEclWp7T1EQhFetXCEz18uIhDEdKKokRgSvq6jWkcIzDUFILylOI5ghDSD4Fs/hPnEuIAaKWix5I91wzbP2GMSM+0/NoHGLG0lSJviXCSCtJs4WGsnRWLGv4uYrjnBOrW5LPvuAbTrfEwGdXssQFSekzJ3b4by07+DYpQ6KtlMaS1hdE7y9NfH2aSf

rS/AdeXfaVd3TeuAZ5v0gbu6bvDdFuM5VweJsyxqNN+ZCmn1UROJ8Ax4xVulF/RtwDLHtvnNeEsB6LG7KVruXbWFP2YN5wrtlwENSh7tNlHs6wCF4EX5sNlJPt8NvtAFnwcd0A05EdHAcRrAxBUBFdcDAcYdiA2ByQKcqdUdrA49Md9dfc0CMCwdsD8c8D1B8dCDiDSBSCbdyDyssZ6dGdmd399J2dOd8Budft+c5dhcKhghxcVQpcZd8AJCFclc

FIVcoh1dNdIVtdQQSo9dUCIB0DKdMCohTZGC2B8CWCOQ2CODkcuC49cAHc2AndWB+C0A3cPd3s/VvcS0/c7pA8uEU4JAVhNAxhVAoBMB+IhAlh8QpRWp0JSA05hwQJ4J4IQJo9JJ1V5pnMg07gNgtZ9EGxLpTFdQARY1bIrgLZB4M9S8HEGslgzoTJcxERK0LE40/EnoyduATJpgP9B5LgnsLo7JC1IAMtMQsskkqQYRyVsF8se0B9xj+00YoZuD

hZKtms59x128iZJ1IsZ9R150NinomZz82ZSg10ekN8t0d0hld99198NCxsJUpkCJmIz9OtFllEH0OFL9uBvhxhU07IP1N1X8TCIRfFFh4EjsINTskD3CIAXYgCPYQCbjENSxfkICAV40i84DwUtdPdED/9kDIBSMkTUB8V6MqMwAaMBNVx6NnA6j7pdEEQdoWitYhiCUujlo6wyj+ifNfDaNWVPchMDARNeV+UaDcT1NRUVM5MdNlNZMlUL928NN

9MQhRtFNSg5VNMNVVT0j44HgggZwKAYS/CTNmoEVBFYIiR8Qu42B2JUwBdsAGIjBnAjAKB+Jrw0ip4qAHgE8LhJgLhzhCEwlGTYDs9n96Tq8c0WNbINgs8noItOiY1oRCFAUU1WihipBLcwxtBVh7oTgERdk2jSgRi0At0qZiR5iUkrJKFFgZj+9XZB9Ssli49sY1i6YDjhUtjp9NjDQ2zWsF8l1WZut11DpAV+shwd9STbsRYxYdNHiz1cAiRXj

l9UB3ifVPiwA1lVQFs0AWSP9i979Jd1t9Y0ByVgSAMWNxhwxbgATg8oSztoMnp4S3ZgDkUpzvlUTkN/k0NwwLFLgTl354DD88MYVCNHzShiS3yyTKMo4qSSg6N1xdltBkzoQaQ0zWSo4Dhcyk1EQxhCyA9+TqS2UcRhMeUxMxSdMsCpT5T5MJTdN5UaLKKRUBcVStVgKNS9N9UdSvSVQDSiJjSvjjNg9moPUWJBJrx2Iq4pofV7MDcfThhtlXFPh

dljFlpLgk0AKnpRzzFLFoRxhxgW40My8nFuZK8MM8LK1GVokeci0sy0AW9fo4lMseyW1Kyu9qyEBay+9CtGyB1mzh0+z58gEuyTK6kXKGk9iJ8OzIAjjOsTjIAzj18xzeZt9rj3zbjZz2KT0pYFy2xlzfQ6LVYqJLhbhvhvEwNH8Tys04yH9jzjY380AkReja9f9IMCTYTny4MSTJwPy74HsAUHpGwYRsSEDQKnlCTZKaCIASRyQIFfkYcpQYAcQ

MhUBT8PRKBqCKgZrSA5qKwFqlqKx9BVq6dsg+CWdBDsgOduURDuAbKOgHNFCpCxc485D3BHreRlCnpVC1cbQNc1SHQdcdD8BNqJBtrdqEB9rlqjq1rfoHCnCXduA3C4CEAvDUsDg+TNyTVhKKhtxdx9xDxjxTwLwrxbx7xHxpK7MpJMjFoARK8nItY0K5hbpjgijUBFgEgHozg5hDLyjdFjLM1PgDhOTzhTgLgrkfN3om9GrK99gtEYRyqkR4RNL

iynLRjwrO9u8O1e9nYCte03Kh8ysWzVjZ92yGZgrDRtjXhdjqsgqMd2tjjhzzjkqt8Jy0qeqMqD9FSnp5zJI5x8ruA1zllVlvjyYX1PM/gaqjy9ZP03hI6GA6qdszENgVpPh7pWroT2qLtYNXyhsb5Pz+qfyrgTgboRqsrFJ8SwKACSMxTyMShyTYKcVfx6NBbuiHorgP8nJtZDyCU/hDhfz5a7JFa41+N4LBMSLhSyLiBxMBUvapNqLFV5NFl0g

PlD1KhagGhmhWhfwIA2dsBQFFo6iaQphEtPErgnIW4Thu7t1cAqoV8KJxRMByKJNDNH0dUGKF6mLlSuK2LZ7IBNTWLmoqb9T8BDSBLMahLvaRL8RSAjAOA5gjBZA2AeBUxrw4A5wxh6BnA2BUxPTz49SFIE9aaEhwk9FvNOTDyIBDoLo3ENgX5U1aw5h8x+bnF6SGimTmjokizIAOi0AOSejuS6wBi2MnoSzUAyyO83LKRJiphpjvK9bisFjh9lj

ShWyTb+zzaalQrGtwrAroqIBYqVz4rKg19RzN9Sgrjc7hYRs5zj83xYJ/ab0KIPjg75sHtZhbICE3hATdRjFzyLkP9jhSVeT06Hyq6GRLtESoLbtwCH5C640k0MyPsy6vtM6FJILa68UYLKNG6aT1w6T6jGSmiI0MK8m+GuS+jBHeSCLGEBT3shSuVJ7p7xTNC56ZMP6y65T2nf7VRmKtSDNun/7v7AGMjgHQH2qTTsaJA4B2IEB0JWpBF5hrwoA

KBsAYRCBYIRh9Aqhi4OAmhcGCIRmCH/M8xpgyUGVoRJjPNWbnBVpZh4FCFGwtE6U3tnotHWHCnmTOGMyeG2aPh+GKnntBjW9nKNGKyFGUlpHZGdbZiGz9amySlR8qsx0zbOyLbuzQXdGUWYr7a4rHakqzHIALHQDRk7j/qj8crJJUi2s5k3jb1nHX6VYdzAN4Q1hVhDtvHAMbg/Hm81hIQ9sQmYSs6ESPlLGwD870S4nvg41S7umUnK6Jr0naF/x

67snaMm68n3nGjPn0yo5nAynmqeTBiR6wBbt8B6mRSn6Z7CrmLpSFS6LOnVMy6qKWKhnZTOKtNuK8Hp5Rn+LxnBKg9IGKhupGBYJnBmBBIs5CA05pdBJsBlA2wjhrxzwhB9mgGjmaaTnbpB4bhax4EHpladgFLoscxNgtYARr8M9mHEzkLLlUybodXG9vDAMcykQcKCySmRHVbSyxjwX3Kmgaz5g6yfK4W/KEWsZjbIr1isWemQqp1rbkWWlDicX

DG8XTHLjUrRWSXMrumfaZYbNqWr0VzA770XHGW1ZLYWMKEt1KrP1PgC347o7E7Ns7pFgLELgBXUmnyImRXiW7txXYmnttldlbgZW6K5XxrYTFWUVcmqUKS4KTX1WqUkKUK62lauGSgsLW38y8LWTjXTXzXGmKKnWbXGKOmORbXaKWnJSXWPWf77X3XtSf6Dn8GlMQHfX5WEAJnA3Qb2Iph8B6AKBUx7TmZmIRhupUwqgeBmB2Jrx6hU3DmnMaaLh

pg7g41Lo5hRa7rKGFLJgJhB431jEv9K2aitHkPa20L62O3bKpbm3sLsP8LgW1bQWNaPKvLoX6y8kR3Fix2Vix9J3TbF3UXNG52dG1HbbsXF8HbV8es13xz+Y3bpzrGy7d23xyaF9D3fRj3XhT3KYmXNg9OLoHK1to7uY46b2n3AN26JgjE47js/8OOhWXzInN2/2+qJXAOTpj6jGknZWK6IP4Ua6lWKNYOG61WYP/wzOUyLO0O2SwBMO8zcL8K8O

x6OUJ7RMp6iPunnWKO3X37HWtvemAHdu+nPXmPvXZU2OjS/XwGA3uEJBupQR5hiB8AoAu0FJlFJrqajoFgDgYyHobpSUyUwNDoWN5hY0rhvgzgzgotqj4yGt08q9W23gtE9loRJam2iuVb60nOguwXkYcse83unzda5je2Db/LEXMXAuZ00WtGwNyyqfF0l8hzouRygSUrXaWuZzPa6KUuCIGIHGtumXKiU0nmKq6qwxThuXdRYynJbpMeNx7zBX

ACmuf3kSxW2uAPP8mxPMKGeuwO+vUBzt3u9CqhEp2CwadCKwgcDqVqagYcDDUAjCGCje8QjrmCnerDWxLQNrTfzfUBLeOBfkbeobVqHe6CsCTDXeDBzDPeSDveLqGcEbNtE+rqudbqxCBchcRdpCXqmBpc3rs+wZPrmF6cfqmB7j1SErAag/ga/epYLe2DwaQ/Dqw+OBHfneo+0oY+PeiD4/ch7dHdncXDcDSB3dkbUaaD0bqnOFTTU5NB8Rup4h

hclgeIQJBIoBnB4JcBJBsBi4oAQJsIKb8I03FPeASj9ONh3htk1gboMyQfLgcy9KX2LgYRhH01aiCmtWOGG32i7LfnW6BrSpkC0crY9u26tSRpCx4BDt5G+PQpKOxHzjs/ONtPRuWUtphUMWYXPRgYxZ7swTG7PF2vFy55Jcd2tjAiHJwPYzYj2dLdcjl3WRqw9keYY4JWnF4ldyYCwaXs/k8y3BX2FDOrm1Qa4q8uqUTXqmiS15vB6aP+QCjiSo

7l0xqRvcCkSUG7QdlWWTWDjk1UFUp8mDJb/sU3Q5gA9Wfzcpi3GAEY14KtTRSAR3W5NNP689fbvRz24yliOX9WjmSyUwMd+mZ3b0hdzGYccuOd3dABQHmCCQtA54CgCfGP5gw/s8lXUCczzA0htonjG6L0VZpbQoQoGaEJEg2C4UwMCZYouj1Szv9hiXbMRj2zgGa08scjUnnAPJ4+cVGE7FAdOzQHotcejPaloORXR4CYuBA8xhu1/bc9K+kyBc

hEPS5UCCqsgoqmGAug+YK0vjBSDe2bysC/0DVaqhYgzx4UwM/AjOoIK/bZ1muv7GJjL0fgSCVKWnfXrIPA4KCwm91A3BIGgHrUscehe4TwVOrJ91YqfYQqIRN7iEi+EgXPrIXz7yF3q6ARXJNBUJl91Cbg04jX10K3D0AzwkRnDWH6u4x+sJL3P/2n7+CAi6ATAOeBqC4AGIgkKYLsEiG8hoh6bXgMcBrbGJb+J0AxHmAoYg8bgCQX4PmEHi/EEQ

uQhrDMAKE0EihBEEoeIyJga1cs2tYnjC085k94WiA3zki32JNC6s6A7RpgP87qM7akXXFqzydoEtt0fQ9Xlux56yC+euAHBpQPPyFVhetYe6AUQlrzCJeboDMuVxWEFoTg1YDaJsKV6ftwmewtXulRRKa8jhaGN4NWGMSJMcMyTQ3sb15x6EAA1D70eFwiIAcYxPmdQEIvCoAafG6mgC06x5gRouGQrrAL6y5fhIIkvpAG+qQidM5IbQrXxBroBk

xsNIfs4RRHj9AKKNDEf7ixFmo0CpAfEOETMBthi4DEJIGnEwgjBu4gidiGnG4gIjnwMlU/gGhzwrRkKl0FuMtlOH39XgSFTDNtAyFzBjg6wKtrwyTLmdbglnLhpmRs7zc22OHC8aIyFGJJe2lIVzoOyqGwspRCA5RmKAaELtass7HYqF1VHhd9Gy7XAacXwF9YOeRA/oSQN55kDcA/EQXll3Jh0DtyasXZCdEuCMMOWW0MrgnWdHwgYQtkc6B+x2

HejhW12P0RrzEGBjawwY0tve3OFjZLhUYiCsoNJLjcMUo3IisNwm4nipuZ4mbphRbYLd22jYZbhpCsGiln6B3aTDtzI6OC7WEww7q6wUkncmOC4v+pdzAaz9Jm6AGACBG6jicGIPEFSKSJBHkiz+d0MyuGGWifBPM9I20VpVeB7JkKoGQhN4k5LVojxqACMIj3Dq14HoFKXka8AzL3iyhBSCoWKIZAk93xNQ6UV+J3o/j5R1PGdrTxC4qjGhqUnA

Z0PAndDIJhAwbDBNJY2MKWMsbqILwtHoTbouZD/EsKfy5hOBx9HzPmD+BzDvanosiZAE6o50Dh/7WiZ/ngRPNQOFwyMYoM+53Czo8YusRADiAXoUxbwl/OmMzFfDoxPw+XE9QLF2jRUQIksdADLEQAKxv1QYauhhF19Exc0lUPYSbFvCkabYyftmQxq6TuO1EeoOeHJQ8R6gw4ZwIQDGDdRBIgiH4G2HoD4AKAgkeTix0XFHQNokwNqXRNmANhP8

qQ84AkBLZLZNgL6exHDzp6JAfgPmEwYC35E/MTohwOYF/h4Gd0wpgoiKVSAQD5p9gMA6oQUlqEyj6hyA38VPjp7zsUpTPKLl0LZ4FTehnPYqduzgllS3wxcJCTQKDoMtcufyUDLsgmD5hr29o5li8ydEglYhFiIFKhVIn9chBvU/Ua1xokoYAUSPQotINGoEZ9Z1dMjENzrpqCsUGg3ie0FmBrQGRBMoRrN2OgHA5a5M+6JTIknEVVuDTawZt2tZ

yTSOAzcjlHKXqvlV64lNODxA1DzA5wW9W9LvX3pHQ6iswYxAkxTp343gb2QljfU6JrRzgVwasB3Qbz31CAj9DbjJOyioTnW6kqEX/Q8GndNJGAbSdd2ekBCIAIweCMoHqC7h2IYwCzDwGUAgR5grUIiCMEkBsQIZ53M/oSh8zIUCyt0RsBMG8yMjvoa0bMFokbAmCfgcdPIdVWIYFpGGB42yGBh+aatz6TYVjFTLAGlCIBT4qYAgA/kfzGZcU5mQ

lKNrsyeZnMjKa0KwHTscpd9PKQLIuJxcipRsgYW3PGziyCIrUKWU41oGyz6BmyFaEXRfh5gOWBaTgQYl8SXQYkd5E7KEwmo9T9hRsw4abJ/KNhMMZw8Mb13kGsSlBdslQS7OozcTR6nEkoI/zCSXzlo1828rBy/4Pzi8+wMwSawsFmtx6oc6SVa2UmRyumDg+SbJNVRHc1J2ir1t4NY6+CIOXYheInOTmpz05vOGSrHi+6EotohwG4ACBbhaJmac

dUci3Cf4zBK0caG6IfJ8kUJEeFlOsFEm8UhT7K6WamW/PKEvif5ko+KZ+IAVyioqCo/8VbUAlZTeZmo/mdqPXbCz4FsEo0fBKlCVSJhTLE6JZTeAohtpRsbgB4ialnj3gUrLTlsMoUdVv2lE/FMnG7EQB4Ib0j6V9J+l/SAZQMkGWDNPgn8MiV8HicWH6n0KnsSIOBGBiYlV85B1sq4RNWsWg0m+VvCGhwEWqh8XiDwmaYH3mq7LbeR1A5emNTEp

9lpnwjPt8Kz4bS/hz1AETtML6PLSxYIr6hCOOmILqxuuc6VNWOV7VTl+yq6UiObGI1URE/DsU9KxovSIAwEMCBBCggwQEISEFCGhCwhLz9FUMpaI2GSAJNaweFNYNfmB7N5WGEYC4H8DuCGcMyZ8lGRElQyLAX4hCxtmjUmCbzryMwXMCyv5HhTIlkUumbLS9RvjYlf8+JQFTAWpTmhXMtJRzIHLM9cpCVCCTAqglwKqJBok6d7XgnwQ0FlimWf6

zlmbJr+kIDujhIbB1LvELcStGQo6kULleuwiid1WiYzLvyT2U4K9m64sKDebC8aVBw4maCsUvChDvwupRg9GV+wZldIu9nrAzotiBxbMCRDRqg5gpBRRawbnKKxs23WObemXrQxV61QOoI0BaBPgd60KLOYfR8ywhY6KdSynL23rKBS5kC+ofXJsEv1DV7cxSYvVzXxzmohajeiWu3qZzfQ+TGkLmDzA1rkyUBEurekbW31UAq0ZxQeNOBzBs2D0

IdXXMtbNNJQzclSa4OO66KvBvFHuX4MNVz9cctEeiExFYgcQuIvEASMJFEjmSj1MQ77htBzJc1wwiIOkZ3VZquSxa2yQhHGk2DrA6V5eJSk1TzAPRPMNc6zk2yTxfAtYJkOhlrHvb8rnOkjIVbZJiVFY4l3nVmd+MAVJLpVioloTT2phSqMlK7LUfixyXQS8lJU5LvBKqB6q5xBqm7karDBNVqwd0ZaAQu+CcCmat+W6B6PtVejupbS51aIK/KQE

SFdDEacxLGnXDCo7E6CiN1VZTLMmVKO6GWhv51SYN+g46HUUQ2VpHmL8asCmrqZprCOjclRXYKcGrke1kTAtevWLUWLhY5a30IfTrBIggy1YQYkyQbVNqN8H67xPmH2CEJjEbLDda2vDlGpMF9FDRXHOc3NQ2oHUeCF1F6jKB+og0YaKNHGifKPNe9Edd5vl6/kyiOYIFJHRLnzrj6Z0UDKpXxn5ddkV9B+lurYS7qXBjHRBYM33U8UfWV3U9Rxv

PXoB04mcbOLnDrkFwi4pccuJXGxVfdAsuyFuIllJRJopW97Q6M4ARDTAHocwRsCLT2TvAfJSZLzIIyMTlUcw97O+Y53AEYanxWGhmaKtw3ir8NiU1RkBNQGkbZVmU+VUuw1HUasltG2BZOXdpWNGNpA5BbgEESsbo4J7BLZMPJi35nshCHCeGDqWRg82Ugu1fVxtnkTVe7Sl1QGNmVPxi88my2RGN9XKb/VamoNRpr4WBr2gp2gxOdurCXawkwai

cHIqkntbnB9mhUslpXqpb2onUHqH1AGhDQRoY0CaKWuHWYgcyvGJyI4sZTkoCE1W6+rVqJTeJSUUWFqcFla2bqM1269hB2sS05qKIea7IKvTS2i6stOWyXflpl1DrPNwwQ+oruhB4UVdx9bHf0mC3xVvxsW2zTuoR17rutB61SXouPWGK1lnHM9XpOgAtw24HcLuL3H7iDxh4o8ceM+q7mEM7F5KTjHnLxnQgyVxRbotWHza2RyqVnSAPSthkRgM

8QGqpqEspEfqWtF0X8jWjvERL7t5Qx7SKvc7DsPxb2hJW0PCpKj6edWUff9o6HNrjG+U1VYVNB2JcIdYsp4rgHPCw6702XBHcL1izmb68OE4vJwMGqnByU7UgCJ1Lx0SafRhO6TQXXdUZ4/yCm5ZSxL9WqbQ1cHZ2Q7O01179KjekAVoLsWJotE7ew+XxkIoM7U1Ic9NW2s0VtN9ugu/NcLvS2ZbxduWqXQVtl0u7SyOZYhbohOCrAr+H+ATbOr9

1a6lsnJe6CZBCgxbedCmVCQ6wc2IGrdyB23WgYd3S7CtKjbA9nNwPJkbIpwDGaDxIMUQ51SqgPXQabkh6ut/TBwa3N1LLyNSJ6oxXHvhVLxu4K8NeBvC3g7w94B8I+CMP1XjLIZkABPEiB0Qrq7gImjPDyLDJLRfZa4/4FhK8Ymcp0vsxhhnjEnfN/+SePSg0RvnPy28PewVfTP73iiPOL2krBKsp6UbgFAE37UAoVV8yoF2SkHQlz3yiyClUOj0

maNpboL2NW5RHZVy2ibR8weEtgZSPvYayAMxEpNCLVE246Y9jXYQS1zoVuqn4Sa7aM/p/hKaFW7+xnTwvp0hqBj1Kdw8tE8O3jvZvh8MP4aNYQHhjUB0imHKD3LLs1XTZg1AGt0i6MtYu7LRLry2cGsDxW13TmW2gaUPEMGo7RnnV1iHm1Eho3R1oS2MGlJWa0PbIdkG9aw9/WnwexxUPDb49IwZiMOHQiSANQpAQSPEXYjOAKoPAARFUEESSBZx

cOhQzitMPDBqwwaZ7C9iO3BM7Drk8lMQdAwwgc2YGrRgsDWgXQPE7jAA3BrRpzTNgNkGk8UJfkPjXKD20Izht8rD7JVn25JelPiOgLeT2U0CUqrn3QLnaQs+jRqp3r5KHi8E7uJvvpam6ijCwGGShq04LDyYJwTgTGQCZIzyFjR9hXCUk0iC86xO9o5xmhChlQU3q0aVTr6OcKA13CykpzsQ7RQwebmKk4yaKEEpTo8IfA3MZqZTLLB1m5Y5mtWM

kc1FHxmOVGdeMyHPWchw9V3L4qDa/jfc7ETvUwjnhMA8QIQDPP2bWLX1S0B6KjJPlhaJjJiMMu8ChA5gaVd0NbSIY/5aMtEcQDDFcBmBsjyULzH5grwFEsmaZEgKKUTxikSjIjijQ2jyfSVxHUlCR4jVRrAnKr59EpwlnqOlMILSpa+3AMUqzVMt4EFiK6HxqqVP4KETUwvNsgRl8DL9TRg2TQulNtHICyPCYCzQp2sLVlRpjZegDN4N8A+WyoPt

bz2Wt9mI4fQwvQS75u9Y+ffdgojjB7zTGYvvRMV+aYA/nZq2ylvitSAvt8I+xhHAt33d6A5ILpBGCydST4j8lpHmy6rcuzGZ88x/wwsbtPeX7SuDh075RX1+VnSZpiFxvihb/P44AL6F4C071As4XwLvfL3pTiIuD9HCyIyFa2JtM2gHpPhGfnCv7k1ARg9QXZN3GIDFwVmVQegK1CMArAag3ceCKQH4gKns9CnKGSUemDH0bD5aGyIrL/WDwW2E

YJjC1IuY+TpjqlO4DV2r2Xj4NdRPwwGb5Xd7ceGtPvZya85KMR9sRsfWRrSkUahT850U4lVi5qql9GRw0XKah1SVRh5+ZCbwGblMsb+N/CYOUeqXsDyrcYCrqngO09FauV5o09Qt9Fg7qJMm44ecBIXdGoU9pyDv0edOf6xuIxry1yU3nBZDNI14K9U3MHBn5F0BmzeGZVTwGHN6iqORHK0UR7EzEel9T8dTMx7jFzUOcDABJCtQghOR97lYsslW

WLEbiNYTcESyzHUhtYcuboi1jnAjt661w84lsN/8bOIV/swKrbSE9IrQ+6K5Ob+249x93Muc+0MVWz7UrPQlc7krXOynllxo7ANudWPC8PG9e+6xy3gR1Ki81aH3YrzE1dTjTN+qTWaZNntHY6VJuOksp6O9XM+FQBsYcXgtTU2b3B14aRdZxCFrqq0lAutMkJPKtpxXIsQoT2mgjyoLFv6lWPYuxiwVN0kfndLkvtirxnY1Q/3PYArBi4mEfEEY

D9rPrCzFI4hWtBtUNhDsDJ2yE9cSC69Qx1YONOsG/RNmBat8nw4EZBZhXJGoo4c91Niliqoj3JmI0lenMYDBTU5pI5kpSPA70r6Rj2lqoAjwThTNLFclVKoh5EbgK0HWEeaqpXAmpNwU/Rc0hJk2r9FNp1aaf9E02Hz5wOiVjM9y2nFNzN+5RUD9v6MObrd4i1cveE3KBbdytaQ8pFsqJnldFt5UPcYsy3VclYsun8qBozS2710qSxCtcJQr7pMK

pSxA37lJAhARgaIh5HQgcAjg8ETALgGYjdx0I0bHiIJCXIWWTD8eCEJdGTyDwPEuiWyxcF3lLjEgmsLWDh0s7vsvr3AQRdGqvmKUxFtJmgvfLwqPzo1t21+cEYmKfzEHINvDWDZDuR3QWUNuVYken1w2usNGtK4vvjvg7Mj2Vtff6gi4ZcA60s+Hcqby4vtcJadXO7e1JREKZgxwHNA1dLvXnHVBOqm1XfatBi7octRiY3Zf29G+rjp2nVxKGMIU

qUQD4RZdFAfTpqMEiqB1IoLSWaQz81sM8bqWsaLVrsZiM/Gbo7RniA8h74wYt+P7WtbGZ+1BqG7j4hiAUwRCcbautomaabsllm8Fsljq30rNblWdF+CXtKtKQgB2EtcQYZUe+44hUTP/69n0N3tp8b7eQevbUHSAxJVOxI0pLw75GiKug/VEz68HQOgh5KfVWtXNViC40Xs1yNp2SlasFliGPhAanVZUvJhzVeMSNFs7DRgQWXeauUS6K9544R0+

mHdWQKb58aR+f0L8UYw1AWPnoH0AIobQy4VANgAVClR8crsJC44DQRMBoYMOIFbxbOUD84LCYqamnGmdMBZnHveZ4s89grO1n94VAJs/YLbOmguz7IPs9/PB8+L+gY5+RZIvnVe76fKi/cposj3tpEtvMdLeVyy3E71fGsbCLOcXPSAVzwHDc84B3PVnaIDZ/71efvOoAnz7i986OdK2l7t01e2rYUvNtYVm9jM8XBGCYBMIuAFYOhEICtRcwrUL

RMxBnlJB6gygOPB9xz0KUfg0wR5vmDCRpCzyYZb4E/xTx/B68Xi+uzXoay2RY0QimyLfkvKsrfrTbZaEpW/zujFghM2B6ybx6RTknz2rk2k9lFT7Ib8VhnrFZwfJHFz4pnUUSwY0kO0b8EyOHlbyNGHt9tD+WWNbCTeIcJobtp86IJPIaWBes0frJfx0tGdMgzoMVm28QiOgKr5h1WxMkcf7XToawlHbfVe3QM8Wrn02AHZrOLH5MwyMLhyGvOml

oriVy3SMqKlvZuer0JNfzOBGuhGmjua0saUW6OlS/OyjmNmeMjujHG1vrdHLMdJnLLWk6PURjqjgAhYBEOAHABlC/IqHz4YEJkAqBlhex9IBgCbgoDMQA7Y5nLGgkveHu96ccfNYdRlDCifbwNgYIdJEAYxhwh1U96OatcTmX3N7994dVgjJSYbhQV97e6t33vvtICsDwB4yAPveyjr8sW+7vcZBZ5AOsCTB5Q/6B+IKqiU5h4g8ZBYI/N4Fwur/

fIeCP+gIjzzcBfMJyPmxw6tQTBdi2kP4H+j3B7eMJmWm+Htj/oDnAdyNJll7jx+4yBal4IMlBGIe/Ch4hJQG+3coQgJVhINOMwBlBTFVAsJJQHpHPPsDWjjrs24YDxA2GVoQAjAxBfQFu4fwEB3cAbzhEJ8OpofKHEgCTy+9ZAkBu7ZFlc654rDzrC0uokgOxDYBSw+PuATQMEC9G+fz3pqZiKSGaikBlAjIAABRlLZnSX8/lsDEZnQAAlCqCdzK

AAwQoCoLF4S8lHkvXLSkViHS9HAsvfhbj/B8JA4fpcnAcYUKhlP74nc5vJgzqmC+heZLsJbAEQHnWq3SgQfXdyvfjcJUHcqiHr9V7sCCQEA2AHIFKCD7TMAvCAILyF6zcER5vhARgPBGIJR5HGbGvGOkC2/6xlc+URXPoDE9nwfV4z5TXNZqBbedve3x45uXAD1QkpwQTLrJC/BAA===
```
%%