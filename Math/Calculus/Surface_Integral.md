Can be thought of as the double integral analogue of the line integral.[^1]  

> [!definition] Surface integral of a scalar field
> Let $f$ be a scalar field and $S$ be a surface then the surface integral is
> $\int \int _{S} fdS$.  
> However, to calculate the integral one has to parameterize $S$.  Therefore 
> let $\mathbf{r}(s,t)$ be the parameterization, where $(s,t)$ varies over region $D$ in the plane.
> Then the formula for the surface integral is;
> $\int \int_{S}fdS=\int \int_{T}f(\mathbf{r}(s,t)) , ds \, dt$
> The magnitude of the cross product of the partial derivatives of $\mathbf{r}(s,t)$, i.e., $\mid\mid \frac{\partial\mathbf{r}}{\partial s} \times \frac{\partial\mathbf{r}}{\partial t}\mid\mid$ is known as the surface element.


> 

![[surfaceIntegral]]

> [!definition] Surface integral of a vector field
> Consider a vector field $\mathbf{v}$ on a surface $S$ where for each $\mathbf{v}(x,y,z)$ is a vector.  If we integrate using only the normal component of the vector to the surface, the result is a scalar called the <span style="color:olive"> Flux</span>.  
> 
> For example if the vector field represented fluid flow it would be the measure of the amount of fluid passing through the surface.
> 
> It is important to note that the maximal flow through the surface occurs when the vector field is <span style="color:olive">normal </span>  to the surface and the minimal or 0 flow through surface occurs if the vector field is completely <span style="color:olive"> tangent </span> to the surface.
> The equation is;
>$$
>\begin{align}
\iint_{S}\mathbf{v}\cdot ds&=\iint_{S}(\mathbf{v}\cdot \vec{n})ds\\ \\
&=\iint_{T}(\mathbf{v}(\mathbf{r(s,t)})\cdot \partial r)
\end{align}
>$$




# Footnotes

[^1]: [Wikipedia](https://en.wikipedia.org/wiki/Surface_integral#Surface_integrals_of_vector_fields)