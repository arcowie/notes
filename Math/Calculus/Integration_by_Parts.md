> [!definition] Integration by Parts 
> is a process that finds the integral of a product of functions in terms of the integral of their derivative and antiderivative[^1]
>**Formula**
>$\int_{a}^b u(x)v^{'}(x) \, dx=u(x)v(x)|_{a}^b - \int_{a}^b u^{'}(x)v(x)  \, dx$


# Footnotes

[^1]: [Wikipedia](https://en.wikipedia.org/wiki/Integration_by_parts)