# Definitions
> [!definition| 1] Multi-Dimensional Array of Numbers
> Tensors are a "grid" of numbers with the rank defining the number of axis
> Scalar: Rank 0, just a number like 1, 2, $\pi$, etc
> Vector: Rank 1 tensor $\begin{bmatrix}
1\\
2\\
5\\
\end{bmatrix}$
Rank 3: tensor would be a solid so numbers along the x, y, z axis.
Rank N: tenor would have N axis. 

 <span style="color:olive">Definition 1 is not useful because it fails to describe what a tensor is, which is a mathematical object that describes geometry </span>

> [!definition| 2] Coordinate Defintion
> A tensor is an object that is <span style="color:#0996F2"> invarient </span> under change of coordinates, and has <span style="color:#0996F2"> components </span> that change in a <span style="color:#0996F2"> special, predictable </span> way under a change of coordinates.

For example:
![[tensorExampleInvariant|850]]

> [!definition|3]  Abstract Definition
> A tensor is a collection of <span style="color:#0996F2"> vectors </span> and <span style="color:#0996F2"> covectors </span> combined together using the <span style="color:#0996F2"> tensor prodcut </span>

> [!definition| 4] In Calculus 
> Tensors as <span style="color:olive"> partial derivatives </span> and <span style="color:olive"> gradients </span> that transdofrm with the <span style="color:olive"> Jacobian Matrix </span> 

## Forward and Backward Transformations:
<span style="color:#0996F2"> Old Basis:  </span>$\{\overrightarrow{e_{1}}, \overrightarrow{e_{2}}\}$ 
<span style="color:olive"> New Basis: </span>  $\{\overrightarrow{\epsilon_{1}}, \overrightarrow{\epsilon_{2}}\}$
Forward transformation, going from <span style="color:#0996F2">Old Basis </span>to the <span style="color:olive">New Basis</span>, requires writing the <span style="color:olive">New Basis</span> in terms of the <span style="color:#0996F2"> old </span>;
![[tensorBasisTransform|500]]
$\overrightarrow{\epsilon_{1}}=2\overrightarrow{e_{1}}+1\overrightarrow{e_{2}}$
$\overrightarrow{\epsilon_{2}}=- \frac{1}{2}\overrightarrow{e_{1}}+\frac{1}{4}\overrightarrow{e_{2}}$
In matrix form the equation is;
$$
\begin{bmatrix}
\overrightarrow{\epsilon_{1}} & \overrightarrow{\epsilon_{2}}
\end{bmatrix}
=
\begin{bmatrix}
\overrightarrow{e_{1}} & \overrightarrow{e_{2}}
\end{bmatrix}
\begin{bmatrix}
2 & -\frac{1}{2}\\ \\
1 & \frac{1}{4}
\end{bmatrix}
$$
Similarly the backward transformation, going from <span style="color:olive">New Basis</span> to the <span style="color:#0996F2">Old Basis </span>, requires writing the <span style="color:#0996F2">Old Basis </span> in terms of the <span style="color:olive">New Basis</span>.
$\overrightarrow{e_{1}}= \frac{1}{4}\overrightarrow{\epsilon_{1}}+(-1)\overrightarrow{\epsilon_{2}}$
$\overrightarrow{e_{2}}=\frac{1}{2}\overrightarrow{\epsilon_{1}}+2\overrightarrow{\epsilon_{2}}$
In matrix form the equation is;
$$
\begin{bmatrix}
\overrightarrow{e_{1}} & \overrightarrow{e_{2}}
\end{bmatrix}
=
\begin{bmatrix}
\overrightarrow{\epsilon_{1}} & \overrightarrow{\epsilon_{2}}
\end{bmatrix}
\begin{bmatrix}
\frac{1}{4} & \frac{1}{2}\\
-1 & 2
\end{bmatrix}
$$
note
> If **F** is the label for the forward transformation matrix and **B** is the label for the backward transformation then;
> **FB**=**I** e.t $\mathbf{F}=\mathbf{B^{-1}}$