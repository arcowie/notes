A Taylor series, also known as a Taylor expansion is the sum of a function at a point and the value of its derivatives at that point.  

If a function has a discrete number of derivatives then this some will also be discrete.  A polynomial would be an example, and in this case the polynomial is also its own Taylor expansion.  

If the function is an infinite number of derivatives then the sum will be infinite, as in the case of $e^x$.

A **Taylor polynomial** is a partial sum of a Taylor series.  If the first $n+1$ terms of the series are taken then the resulting polynomial is of degree $n$. 

#### Definition
---
For an infinitely differentiable function real or complex valued function $f(x)$ about the point $a$ the Taylor series is given by
$$
f(x)=f(a)+\frac{f^1(a)}{1!}(x-a)+\frac{f^2(a)}{2!}(x-a)^2+\frac{f^3(a)}{3!}(x-a)^3+\dots=\sum_{n=0}^{\infty}\frac{f^n(a)}{n!}(x-a)^n 
$$
where $f^i$ is the $i^{th}$ derivative of $f(x)$.

an alternative way to write this is to consider a perturbation about $x$, $\Delta x$. This relates to the above definition by $\Delta x=x-a$. 
$$
f(x+\Delta x)=f(x)+\frac{f^1(x)}{1!}(\Delta x)+\frac{f^2(x)}{2!}(\Delta x)^2+\frac{f^3(x)}{3!}(\Delta x)^3+\dots=\sum_{n=0}^{\infty}\frac{f^n(x)}{n!}(\Delta x)^n
$$ ^alternateTSForm


#### Example
---
Taylor series for $e^x$ for $x=0$.
$$
e^x=e^0+\frac{e^0}{1}x+\frac{e^0}{2}x^2+\frac{e^0}{6}x^3+\dots
$$
$$
=1+x+\frac{x^2}{2}+\frac{x^3}{6}+\dots
$$
For more informaiton see the [Wikipedia Page](https://en.wikipedia.org/wiki/Taylor_series)


