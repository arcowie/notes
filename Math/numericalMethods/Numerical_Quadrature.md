
## General rule
---
Replace definite integral by a sum of the integrand evaluated at certain points, called quadrature points, multiples by a number, called quadrature weights.
$$
\\\int_{a}^b f(x)dx  \approx w_{1}f(q_{1})+w_{2}f(q_{2})+\dots+w_{n}f(q_{n})=\sum_{i=1}^nf(q_{i})w_{i}  
$$
where the $w_{i}$ are the weights and the $q_{i}$ are the quadrature points and n is the number of quadrature points.

## Newton-Cotes Quadrature Formulas
---
These formulas use evenly spaced quadrature points.  [Interpolate](Interpolation.md) quadrature points and integrate to get the weights.

There are two basic types:

**Open type:** Doesn't use the endpoints of the interval as quadrature points.

**Closed Type:** Uses the endpoints as quadrature points.

**Determine weights** use a [Lagrange polynomial](Lagrange_Polynomial.md) to interpolate $f(x)$ at the quadrature points and integrate the interpolant exactly to get the weight.  This is why Newton-Cotes quadrature rules are called interpolatory.  

#### Midpoint rule 
----
Given an integral $\int _{a}^{b} \, dx$, the quadrature point is $q_{0}=\frac{a+b}{2}$.  This is the mid point of $[a,b]$ and the weight $w_{0}=b-a$, the length of the interval. 

To derive the formula evaluate $f(x)$ at $q_{0}$, i.e. $f\left( \frac{a+b}{2} \right)$, so the **node** is $\frac{a+b}{2}$ and the **value** is  $f\left( \frac{a+b}{2} \right)$.  The Lagrange interpolating polynomial is simply the constant Lagrange interpolating polynomial

$$
\int_{a}^b f(x) \, dx \approx \int_{a}^b \,  f\left(  \frac{a+b}{2} \right)dx=f\left( \frac{a+b}{2} \right)x\Biggr\vert_{a}^b=(b-a)f\left( \frac{a+b}{2} \right)
$$  
#midpointQuadratureFormula
so the **weight** is $b-a$.

#### Trapezoidal Rule
---
This is closed  rule that uses two end points $x=a,b$ to determine the linear [Lagrange polynomial](Lagrange_Polynomial.md).

To derive the formula if the interval is $\{a,b\}$ as 
- **Nodes** $\{a,b\}$  as the 
- **Values** $\{f(a),f(b)\}$
- To obtain the Lagrange polynomial line through $\{(a,f(a)), (b,f(b))\}$.
$$
\begin{equation}
l_{0}=\frac{x-b}{a-b}
\end{equation}
$$
$$
l_{1}=\frac{x-a}{b-a}
$$
$$
L_{1}(x)=f(a)l_{0}+f(b)l_{1}=f(a)\frac{x-b}{a-b}+f(b)\frac{x-a}{b-a}
$$
$$
=\frac{-f(a)x+f(a)b+f(b)x-f(b)a}{b-a}=\frac{[f(b)-f(a)]x+f(a)b-f(b)a}{b-a}
$$
$$
=\frac{[f(b)-f(a)]x+f(a)a-f(a)a+f(a)b-f(b)a}{b-a}
$$

$$
=\frac{[f(b)-f(a)]x-[f(b)-f(a)]a -f(a)a+f(a)b}{b-a}
$$
$$
=\frac{[f(b)-f(a)](x-a)+(b-a)f(a)}{b-a}=\frac{[f(b)-f(a)](x-a)}{b-a}+\frac{(b-a)f(a)}{b-a}
$$
$$
=\frac{(f(b)-f(a))}{b-a}(x-a)+f(a)
$$
With the above approximation of $f(x)$ we integrate to get the formula.
$$
\int_{a}^{b} f(x)\, dx \approx \int_{a}^{b}L_{1}(x) \, dx  =\int_{a}^{b} \frac{(f(b)-f(a))}{b-a}(x-a)+f(a) \, dx 
$$
$$
=\frac{(f(b)-f(a))}{b-a}\frac{(x-a)^{2}}{2}\Biggr|_{a}^{b} +f(a)x\Biggr|_{a}^{b}
$$
$$
=\frac{(f(b)-f(a))}{b-a}(\frac{(b-a)^{2}}{2}-0) +(b-a)f(a)=(b-a)\frac{f(a)+f(b)}{2}
$$

Therefore the weights $w_{0}=w_{1}=\frac{b-a}{2}$.  Therefore the approximation of the area under the curve is approximated using a trapezoid.

#### Simpsons Rule
---
Simpson's rule is another closed [[#Newton-Cotes Quadrature Formulas|Newton-Cotes]] rule.   It uses 3 points instead of two, the end points of the interval and the midpoint.  Thus if the interval is $[a,b]$ then:
- **Nodes:** $\{a,\frac{(b-a)}{2},b\}$
- **Values:** $\{f(a),f(\frac{(b-a)}{2}),f(b)\}$
- To obtain the quadratic Lagrange polynomial passing through, $\{(a,f(a)),(\frac{b-a}{2},f(\frac{b-a}{2})),(b,f(b))\}$

$$
l_{0}=\frac{x-\frac{b-a}{2}}{a-\frac{b-a}{2}}\frac{x-b}{a-b}
$$
$$
l_{1}=\frac{x-a}{\frac{b-a}{2}-a}\frac{x-b}{\frac{b-a}{2}-b}
$$
$$
l_{2}=\frac{x-a}{b-a}\frac{x-\frac{b-a}{2}}{b-\frac{b-a}{2}}
$$
$$
L_{2}(x)=f(a)\frac{x-\frac{b-a}{2}}{a-\frac{b-a}{2}}\frac{x-b}{a-b}+f(\frac{b-a}{2})\frac{x-a}{\frac{b-a}{2}-a}\frac{x-b}{\frac{b-a}{2}-b}+f(b)\frac{x-a}{b-a}\frac{x-\frac{b-a}{2}}{b-\frac{b-a}{2}}
$$
if we substitute $h=b-a$ then the equation above can be rewritten as

$$
L_{2}(x)=\frac{2f(a)}{h^2}(x-\frac{a+b}{2})(x-b)-\frac{4f(\frac{a+b}{2})}{h^2}(x-a)(x-b)+\frac{2f(b)}{h^2}(x-a)(x-\frac{a+b}{2})
$$

Integrating the first term to get the weight;
$$
w_{0}=\int_{a}^{b}\frac{2}{h^2}(x-\frac{a+b}{2})(x-b) \, dx 
$$
$$
=\frac{2}{h^2}\int_{a}^{b}x^2-(\frac{a+b}{2})x-bx+\frac{ab+b^2}{2} \, dx 
$$
$$
=\frac{2}{h^2}\Biggr[\Biggr(\frac{x^3}{3}-\frac{(a+b)}{4}x^2-\frac{bx^2}{2}+\frac{ab+b^2}{2}x\Biggr)\Biggr|_{a}^{b}\Biggr]
$$
$$
=\frac{2}{h^2}\Biggr(\frac{b^3}{3}-\frac{ab^2+b^3}{4}-\frac{b^3}{2}+\frac{ab^2+b^3}{2}-\frac{a^3}{3}+\frac{a^3+a^2b}{4}+\frac{a^2b}{2}-\frac{a^2b+ab^2}{2}\Biggr)
$$
$$
=\frac{2}{h^2}\frac{b^3-3ab^2+3a^2b-a^3}{12}=\frac{2}{h^2}\frac{h^3}{12}=\frac{h}{6}
$$
So weight $w_{0}=\frac{h}{6}$.  Repeating this for the two remaining terms results in $w_{1}=\frac{2h}{3}$ and $w_{2}=\frac{h}{6}$.

#### Error analysis #errorAnalysis
---
We will use the [[Taylor_Series]] to analyze the error of the above rules.  First consider the [[Numerical_Quadrature#Midpoint rule|Midpoint Rule]]:
$$
\int_{a}^b f(x) \, dx \approx (b-a)f\left( \frac{a+b}{2} \right)
$$
Therefore, the residual or error($E_{midPt}$) would be given by:

$E_{midPt}=\biggr|\int_{a}^{b} f(x)\, dx-(b-a)f\left( \frac{a+b}{2} \right)\biggr|$

First we use what is sometimes called the [First Fundamental Theorem of Calculus](https://en.wikipedia.org/wiki/Fundamental_theorem_of_calculus#First_part), which tells that there is a function $F(x)$ s.t.
$$
\int_{a}^x f(s)  \, ds=F(x) 
$$
Applying this to our error formula for the Midpoint rule, $E_{midPt}$, $F(b)$ is what we need.  

$$
\int_{a}^b f(s)\, ds=F(b)
$$

^87f7fe

Now we will combine our result from applying the Fundamental Theorem of Calculus, equation [[#^87f7fe]], with the [[Taylor_Series]] .  Specifically we will use the [[Taylor_Series#^5c226d|alternate Taylor Series expansion]], the $x+\Delta x$ form.  Therefore, $b$ will be replaced by $a+h$, and our integral becomes

$$
\int_{a}^{a+h} f(s)\, ds=F(a+h)=F(a)+F^1(a)h+F^2(a)\left( \frac{h^2}{2!} \right)+F^{3}\left( \frac{h^3}{3!} \right)+\dots 
$$

^87074b

Applying the [Second Fundamental Theorem of Calculus or the Newton-Leibniz theorem](https://en.wikipedia.org/wiki/Fundamental_theorem_of_calculus#Second_part) to equation [[#^87074b]];
$$ 
F(a)=0 \tag{$\ast$}
$$
$$
F^1(a)=f(a)
$$
$$
F^{2}(a)=f^1(a)
$$
$$
F^3(a)=f^2(a)
$$
$$
\dots
$$
$$
\int_{a}^{a+h} f(s)\, ds=F(a+h)=0+f(a)h+f^1(a)\left( \frac{h^2}{2!} \right)+f^{2}\left( \frac{h^3}{3!} \right)+\dots 
$$

^881559


Note($\ast$) $F(a)$ is $\int_{a}^a f(s) \, ds$, which must be $0$.

Next we expand the midpoint rule using $b=a+h$ 
$$
(b-a)f\left( \frac{a+b}{2} \right)=f\left( \frac{a+a+h}{2} \right)=(a+h-a)f\left( \frac{2a+h}{2} \right)=hf\left( a+\left( \frac{h}{2} \right) \right)
$$

$$
hf\biggr(  a+\biggr( \frac{h}{2}\biggr)\biggr)=h \biggr[f(a)+f^1(a)\frac{h}{2}+f^2(a)\frac{h^2}{4\cdot2!}+f^3(a)\frac{h^3}{8\cdot3!}+\dots \biggr]
$$

$$
=f(a)h+f^1(a)\frac{h^2}{2}+f^2(a)\frac{h^3}{16}+f^3(a)\frac{h^4}{64}+\dots
$$

^111d91

Subtracting equation [[#^881559]] from equation [[#^111d91]] gives,
$$
E_{midPt}=f(a)h+f^1(a)\left( \frac{h^2}{2!} \right)+f^{2}\left( \frac{h^3}{3!} \right)+\dots-\biggr[f(a)h+f^1(a)\frac{h^2}{2}+f^2(a)\frac{h^3}{8}+f^3(a)\frac{h^4}{12}+\dots \biggr]
$$

Distributing and grouping similar terms,
$$
E_{midPt}=\biggr[f(a)h-f(a)h\biggr]+\biggr[f^1(a)\biggr(\frac{h^2}{2}\biggr)-f^1(a)\biggr(\frac{h^2}{2}\biggr)\biggr]+\biggr[f^{2}\biggr( \frac{h^3}{8}\biggr)-\biggr[f^{2}\biggr( \frac{h^3}{64}\biggr)\biggr]+O(h^4)
$$

$$
=0+0+f^2(a)\biggr(\frac{7h^3}{64}\biggr)+O(h^4)
$$

^43da0a

The first term that does not cancel is our [Taylor Remainder](https://en.wikipedia.org/wiki/Taylor%27s_theorem#Estimates_for_the_remainder), and from equation [[#^43da0a]] that term is, $f^2(a)\biggr(\frac{7h^3}{64}\biggr)$.  The only variable in that term is $h^3$ and therefore, our error is $O(h^3)$. #result

An important consequence of the above result is that the Midpoint rule will give the exact answer for linear functions because the second derivative a linear function is $0$.   However, the Midpoint rule will not give the exact solution for quadratic or higher functions. #interestingFact 



