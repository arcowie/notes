Lagrange Polynomial: **unique polynomial of lowest degree that interpolates a set of points.**  #Lagrange_Polynomial

Let $(x_{i},y_{i})$ be tuple with $0\leq i\leq k$ where $i , k \in N,$  $x_{i}$ are called **nodes** and $y_{i}$ are called **values**, then the Lagrange Polynomial is $L(x)$ and has degree $\leq k.$  Also $L(x_{i})=y_{i}$ $\forall x_{i},y_{i}.$

### Definition:
---
For $n+1$ nodes $\{x_{0},x_{1},\dots, x_{n+1}\}$, which must be unique i.e. $x_{i}\neq x_{k}$ if $i\neq k$ the Lagrange basis for a degree $\leq n$ is a set of polynomials $\{l_{0}(x),l_{1}(x),\dots,l_{n}\}$.  Each polynomial is of degree $n$, and has the value $l_{i}(x_{m})=\delta_{im}$, i.e. $=0$ when $i\neq m$  and $1$ when $i=m$.

### Formula for basis polynomial
$$l_{i}(x)=\frac{x-x_{0}}{x_{i}-x_{0}}\frac{x-x_{1}}{x_{i}-x_{1}}\dots\frac{x-x_{i-1}}{x_{i}-x_{i-1}}\frac{x-x_{i+1}}{x_{i}-x_{i+1}}\dots\frac{x-x_{n}}{x_{i}-x_{n}}=\prod_{0\leq m\leq n}\frac{x-x_{m}}{x_{i}-x_{m}}$$

The Lagrange interpolating polynomial for the **nodes** passes through the **values** $\{y_{0},y_{1},\dots,y_{n+1}\}$ . Therefore the **values** are the weights...
$$
L(x)=\sum_{i=0}^{n}y_{i}l_{i}(x)
$$
### Example
Consider points $(1,2),(3,4),(6,8)$.  We have 3 **nodes** and 3 **values**.
Nodes: $\{1,3,6\}$
Values: $\{2,4,7\}$
$$
l_{0}=\frac{x-3}{1-3}\frac{x-6}{1-6}
$$
$$l_{1}=\frac{x-1}{3-1}\frac{x-6}{3-6}$$
$$l_{2}=\frac{x-1}{6-1}\frac{x-3}{6-3}$$

$$
L(x)=2l_{0}+4l_{1}+7l_{2}
$$

For more information see the [Wikipedia page](https://en.wikipedia.org/wiki/Lagrange_polynomial)