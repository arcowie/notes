**Definition:** Given two positive numbers *x* and *y* there is an integer *n* such that $nx>y$. #definition 

**Meaning:**  There are no infinitely large or small numbers, ie the natural numbers are not bounded above.

For more information see the [Wikipedia article](https://en.wikipedia.org/wiki/Archimedean_property)