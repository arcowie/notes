Given a triangle with sides *a*, *b*, *c*
![[triangle]]
then $a+b\geq c$

- We can extend this to absolute values where $a\text{ and }b \in \mathbb{R}$ $$|a+b|\leq|a|+|b|$$
- We can extend this to vectors by considering the sides of the triangle as vectors so let $\mathbf{a}\text{ and }\mathbf{b}$ be vectors $$|\mathbf{a}+\mathbf{b}|\leq|\mathbf{a}|+|\mathbf{b}|$$

- This also appears as one of the axioms defining norms
$$
\Vert \mathbf{a}+ \mathbf{b}\Vert\leq\Vert\mathbf{a}\Vert+\Vert\mathbf{b}\Vert
$$

- **Reverse Triangle Inequality**  $$
|a|-|b|\leq|a-b|
$$
#definition