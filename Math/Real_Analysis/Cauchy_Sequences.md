> [!definition|*] Cauchy Sequence
> A sequence $\{a_{n}\}_{n=1}^{\infty}$ is called a *Cauchy Sequence* if for every $\epsilon>0$ there is an $N\in\mathbb{N}$ s.t. if $m,n\geq N$ then $|a_{m}-a_{n}|<\epsilon$ #definition 
> ^cauchy-definition

**Note:** Can be useful for showing a series converges without knowing the limit to which it converges to.

**Graphical interpretation:**
![[cauchySequenceDrawing]]
