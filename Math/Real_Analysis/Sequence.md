
> [!definition] Sequence
> A sequence is a function whose domain is $\mathbb{N}$, in other words $$
a:\mathbb{N}\to\mathbb{R}
$$ Often written as $a_{n}=a(n)\text{ where } n\in\mathbb{N}$, also often thought of in terms of a list of numbers $\{a_{1},a_{2},a_{3}\dots\}$
#definition 


> [!definition] Convergence of a Sequence
> We say a sequence $\{a_{n}\}_{n=1}^{\infty}$ converges to $L$ if for every $\epsilon>0$ there is an $N\in\mathbb{N}$ st. $|a_{n}-L|<\epsilon \text{ for }n\geq N$.  This is often written as $\lim_{ n \to \infty } a_{n}=L$.
#definition
> 

*Note* a sequence that does not converge is said to be diverge.

**Outline for proofs**
![[epsilonProofOutlineForSequence]]
**Example 1:**
___
<span style="color:orange"><u>Prove</u></span>: $\lim_{ n \to \infty } \frac{1}{n^{2}}=0$
<span style="color:olive">Scratch Work:</span>
$$
\begin{align}
\mid\frac{1}{n^{2}}-0\mid &< \epsilon \\
\mid \frac{1}{n^{2}}\mid &< \epsilon &&\text{simplified by removing 0}\\
\frac{1}{n^{2}} &<\epsilon &&\text{has to be postive so removed abs value} \\
n^{2} &> \epsilon &&\text{took reciprical so flipped the inequality} \\
n &> \sqrt{ \frac{1}{\epsilon}} &&\text{This will be our N}
\end{align}
$$



`\begin{proof}`
Given $\epsilon>0$ take $N, n\in\mathbb{N}$ st. $N>\lceil  \sqrt{ \frac{1}{\epsilon} }\rceil$ by the [[Archimedean_property]]  <span style="color:#F36E0F"> Note: </span> *we took the ceiling because N has to be a natural number!* 
	
Then if $n\geq N$ then
	
$$
\begin{align} 
n&>\sqrt{\frac{1}{\epsilon} } \\
n^{2}&>\frac{1}{\epsilon} \\
\frac{1}{n^{2}}&<\epsilon \\
\mid \frac{1}{n^{2}}\mid &<\epsilon \\
\mid \frac{1}{n^{2}}-0\mid &< \epsilon\\
\end{align} 
$$
so $\lim_{ n \to \infty } \frac{1}{n^{2}}=0$

`\end{proof}`




