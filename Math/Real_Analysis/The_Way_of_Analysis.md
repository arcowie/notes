# Index
## [[#Chapter 2]]
## Chapter 3
## Chapter 4


### Chapter 2 Construction of the Real Number System
___
<span style="color:olive"> 2.1 </span> [[Cauchy_Sequences|Cauchy Sequences]]
	- Problem:
			- Construct a mathematical system, by means of precise, unambiguous definitions and theorems proved by pure logical reasoning, accepting as given the logic, set theory, and rational number system discussed in the previous chapter, which has as much as possible in common with the intuitive real number system.[^1]
	- Solution: <span style="color:olive">  </span> <span style="color:olive">Cauchy completion of the rationals  </span>[^2] 
		-  Properties we want our solution to have
			- Contain the rational numbers
			- Similar arithmetic properties to the rational numbers
			- Similar order
			- We want the real number system to be an ordered field[[Field#Ordered Field]]
			- We want the reals to be complete, i.e. contain numbers that are not rational numbers such as $\sqrt{ 2 }, e,\pi\dots$
			- We want the rationals to be dense in the reals, meaning that for any two real numbers $a,b$ there exists a rational number $y$ such that $a<y<b$.
				- Another way of thinking of this is that there is always a rational number arbitrarily close to a real number, think of making the interval $[a,b]$ ever smaller.
	
![[Cauchy_Sequences#^cauchy-definition]]

> [!proposition|*] Equivalence of Cauchy sequences
> Two Cauchy sequences are aid to be equivalent if they have the same limit.
> To formally call it an equivalence relationship it has to have the following properties:
> 	- Reflexivity
> 	- Symmetry
> 	- Transitivity
> 	The first two are obvious for the third see Lemma 2.1.1 and subsequent proof


> [!lemma|2.1.1] 
> The equivalence of Cauchy sequences of rationals is transitive

`\begin{proof}`
Let $x_{1},x_{2,x_{3}},\dots,$ $y_{1},y_{2},y_{3},\dots$, and $z_{1},z_{2},z_{3},\dots$ by Cauchy sequences of rational numbers s.t. $x-y$ and $y-z$ pairs are equivalent.  To show that $x-z$ pair is equivalent we show that given any $\epsilon>0$, we can find n,$N_{\epsilon}\in \mathbb{N}$ s.t. for all $n>N_{\epsilon},|x_{n}-z_{n}|<\epsilon$.<br>
By the $x-y$ equivalence there exists $N_{\epsilon,1}$ s.t. for $n>N_{\epsilon,1},|x_{n}-y_{n}|< \frac{\epsilon}{2}$.  Similarly By the $y-z$ equivalence there exists $N_{\epsilon,2}$ s.t. for $n>N_{\epsilon,2},|y_{n}-z_{n}|< \frac{\epsilon}{2}$.  Taking the $N_{max}$ to be the max of $N_{\epsilon,1}$ and $N_{\epsilon,2}$ for all $n>N_{max}$
$$
\begin{align}   \\
|x_{n}-z_{n}| = |(x_{n}-y_{n})+(y_{n}-z_{n})|\leq |x_{n}-y_{n}|+|y_{n}-z_{n}|<  \frac{\epsilon}{2} + \frac{\epsilon}{2}=\epsilon
\end{align}  $$
by the triangle inequality.
`\end{proof}`
> [!definition] 2.1.1
> Let $C$ denote the set of all Cauchy sequences $x_{1},x_{2},x_{3}\dots$ of rational numbers, and let $\mathbb{R}$ denote the set of equivalence classes of elements of $C$.  We call $\mathbb{R}$ the real number system, or the reals and the elements of $x$ of $\mathbb{R}$ are called the real numbers.  Also we will say that a particular Cauchy sequence in the equivalence class $x$ converges to or has $x$ as a limit.
> <br>
> In other words the label for each equivalence class $x$ is the real number that the Cauchy sequences converge to.



2.1.3
___
> [!exercise|2]  
> Show that every real number can be given by Cauchy sequence of rationals $r_{1}, r_{2}, r_{3},\dots$ , where none of the rationals are integers.

<span style="color:#F36E0F"> Proof concepts: </span>
	Use the density of the rational numbers.
`\begin{proof}`
Let $x$ be any real number and $n,i,j$ be natural numbers.  Since the $\mathbb{Q}$ numbers are dense in $\mathbb{R}$ we can choose $r_{1}, r_{2}, r_{3,\dots}\in\mathbb{Q}$ s.t. $|x-r_{1}|< \frac{1}{2}$, $|x-r_{2}|< \frac{1}{4}$, $|x-r_{3}|< \frac{1}{8}$, forming a sequence where $|x-r_{n}|< \frac{1}{2^n}$ and $r_{1}, r_{2}, r_{3,\dots}$ are not integers.  
		<span style="color:#F40C03"> Show the Sequence converges:</span>
		By definition a sequence $\{a_{n}\}^{\infty}$ converges to L if for all $\epsilon>0$ there exists $m,n,N_{\epsilon}\in \mathbb{N}$ s.t. if $n>N_{\epsilon}$ then $|a_{n}-a_{m}|<\epsilon$.
		For the sequence defined above  $|x-r_{n}|< \frac{1}{2^{n}}$ we choose $N_{\epsilon}$ s.t. $\frac{1}{2^{N_{\epsilon}}}<\epsilon$
		 Therefore for $n>N_{\epsilon}$, $|x-r_{n}|< \frac{1}{2^{n}}< \frac{1}{2^{N_{\epsilon}}}<\epsilon$, and therefore converges to $x$ our real number.  Furthermore since all convergent series are Cauchy, this is a Cauchy sequence

`\end{proof}`
# Footnotes

[^1]: Strichartz, Richard. *The Way of Analysis.* Jones and Bartlett Publishers 
[^2]: There are equivalent solutions discussed later