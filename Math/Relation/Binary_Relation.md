Associates elements of one set called the domain with elements of another set called the range or codomain.

## Definition:
- Let $\mathbf{X}$ and $\mathbf{Y}$ be sets with the Cartesian product $\mathbf{X}\times\mathbf{Y}=\{(x,y)\vert x\in\mathbf{X}\text{ and } y\in \mathbf{Y}\}$ and the elements called ordered pairs.
 - A binary relation $\mathbf{R}$ over sets $\mathbf{X}\text{ and }\mathbf{Y}$ is a subset of $\mathbf{X}\times\mathbf{Y}$.
	 - $(x,y)\in\mathbf{R}$ reads  $x$ is $\mathbf{R}$-related to $y$ and is denoted by $x\mathbf{R}y$
	 - The domain of $\mathbf{R}$ is the set of all $x$ such that $x\mathbf{R}y$ has at least one $y$ .
	 - The codomain/range/image of $\mathbf{R}$ is the set of all $y$ such that  $x\mathbf{R}y$ has at least one $x$.
	 - **Note** the elements for $R$ are ordered pairs.
- **Homogeneous relation(endorelation)** is when $\mathbf{X}=\mathbf{Y}$, ie $\mathbf{X}\times\mathbf{X}$.
- **Heterogeneous relation** $\mathbf{X}\text{ and }\mathbf{Y}$ maybe different sets.
## Operations:
- ### Union:
	- Let $\mathbf{R}\text{ and }\mathbf{S}$ be binary relations over sets $\mathbf{X}\text{ and }\mathbf{Y}$ then $\mathbf{R}\cup \mathbf{S}=\{(x,y)\vert x\mathbf{R}y\text{ or } x\mathbf{S}y\}$
		-  **Ex**: $\leq$ is the union of < and =.  
- ### Intersection:
	- Let $\mathbf{R}\text{ and }\mathbf{S}$ be binary relations over sets $\mathbf{X}\text{ and }\mathbf{Y}$ then $\mathbf{R}\cap \mathbf{S}=\{(x,y)\vert x\mathbf{R}y\text{ and } x\mathbf{S}y\}$
		- **Ex**: The relation *is divisible by 6* is the intersection of the relations *is divisible by 3* and *is divisible by 2*.
- ### Composition:
	- Let $\mathbf{R}$ be a binary relation over sets $\mathbf{X}\text{ and }\mathbf{Y}$ and $\mathbf{S}$ be a binary relation over sets $\mathbf{Y}\text{ and }\mathbf{Z}$ then $\mathbf{S}\circ \mathbf{R}=\{(x,z)\vert \exists y \text{ such that } x\mathbf{R}y\text{ and } y\mathbf{S}z\}$
	- **Ex**: *Grandmother of* would be the composition of the relation *is the mother of* $\circ$ *is the parent of* .
- ### Converse:
	- if $\mathbf{R}$ is a binary relation over sets $\mathbf{X}\text{ and } \mathbf{Y}$ then $\mathbf{R^{T}}=\{(y,x)\vert x\mathbf{R}y\}$
	- **Ex**: = and $\neq$ are their own converses, < and > are each other's converses.
- ### Complement:
	- if $\mathbf{R}$ is a binary relation over sets $\mathbf{X}\text{ and } \mathbf{Y}$ then $\mathbf{\bar{R}}=\{(x,y)\vert\neg x\mathbf{R}y\}$

- ### Matrix representation
	- Binary relations over sets $\mathbf{X}\text{ and }\mathbf{Y}$ can be represented by logical matrices, a matrix with only 0 or 1.
	- **Ex**:
		- $\mathbf{A}=\{\text{anime, action, drama, comedy}\}$
		- $\mathbf{B}=\{\text{Al, Nick, Shawn, Dave}\}$
		- Relation on $\mathbf{A}\times\mathbf{B}$ *is owned by* $\mathbf{R}=\{\text{(anime, Al), (anime, Shawn), (comedy, Nick), (action, Al),(drama,Dave)}\}$
		- $$
\begin{bmatrix}
\frac{\text{A}}{\text{B}}&\text{anime}&\text{action}&\text{drama}&\text{comedy}\\ 
\text{Al}&1&1&0&0\\ 
\text{Nick}&0&0&0&1\\ 
\text{Shawn}&1&0&0&0 \\
\text{Dave}&0&0&1&0
\end{bmatrix}
$$
- ### Types of relations:
	- See [Wikipedia](https://en.wikipedia.org/wiki/Binary_relation) article.
- ### Homogeneous relations
	- Let $\mathbf{X}$ be a set with a binary relation $\mathbf{R}$ over it s.t. $\mathbf{R}\subseteq \mathbf{X}\times\mathbf{X}$.
	- This can be represented as directed simple graph with loops where members of $\mathbf{X}$ are the vertices and the edges come from $\mathbf{R}$
	- Properties of relation $\mathbf{R}$ over set $\mathbf{X}$
		- [Reflexive](https://en.wikipedia.org/wiki/Reflexive_relation "Reflexive relation"): $\forall x \in \mathbf{X}, x\mathbf{R}x$
		- [Irreflexive](https://en.wikipedia.org/wiki/Irreflexive_relation "Irreflexive relation"): $\forall x \in \mathbf{X},\text{ not }  x\mathbf{R}x$ 
		- _[Symmetric](https://en.wikipedia.org/wiki/Symmetric_relation "Symmetric relation")_: $\forall x,y \in \mathbf{X}\text{ if } x\mathbf{R}y \text{ then } y\mathbf{R}x$
		- _[Antisymmetric](https://en.wikipedia.org/wiki/Antisymmetric_relation "Antisymmetric relation")_: $\forall x,y \in \mathbf{X},\text{ if } x\mathbf{R}y \text{ and } y\mathbf{R}x \text{ then }x=y$
		- _[Asymmetric](https://en.wikipedia.org/wiki/Asymmetric_relation "Asymmetric relation")_: $\forall x,y \in \mathbf{X}, \text{ if } x\mathbf{R}y \text{ then not } y\mathbf{R}x$.
		- _[Transitive](https://en.wikipedia.org/wiki/Transitive_relation "Transitive relation")_: $\forall x,y,x \in \mathbf{X}, \text{ if } x\mathbf{R}y \text{ and } y\mathbf{R}z \text{ then } x\mathbf{R}z$
		- _[Connected](https://en.wikipedia.org/wiki/Connected_relation "Connected relation")_: $\forall x,y \in \mathbf{X}, \text{ if } x\neq y \text{ then } x\mathbf{R}y \text{ or } y\mathbf{R}x$.
		- _[Strongly connected](https://en.wikipedia.org/wiki/Connected_relation "Connected relation")_:$\forall x,y \in \mathbf{X}, x\mathbf{R}y \text{ or } y\mathbf{R}x$.
		- _[Dense](https://en.wikipedia.org/wiki/Dense_order#Generalizations "Dense order")_: $\forall x,y \in \mathbf{X}, \text{ if } x\mathbf{R}y \text{ then }\exists z \in \mathbf{X} \text{ s.t. } x\mathbf{R}z \text{ and } z\mathbf{R}y$.
- For Closure please see note on DAGs
- Information for this note came from the Wikipedia article on [Binary Relations](https://en.wikipedia.org/wiki/Binary_relation)