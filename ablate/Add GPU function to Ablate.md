## Problem:
	libCeed reads the entire that has the kernal and submitts it to NVTRC for JIT compilation.  NVRTC does not currently recognize outside headers.

## Solution:
1. [x] Create my own fork. ✅ 2024-11-04
2. [x] Find file and the function that calls the function to calculate the residual ✅ 2024-11-04
	- **File** [finteVolumeSolver.cpp](/home/arcowie/repos/ablate/src/finiteVolume/finiteVolumeSolver.cpp)
	- **Function** [PetscErrorCode ablate::finiteVolume::FiniteVolumeSolver::ComputeRHSFunction(PetscReal time, Vec locXVec, Vec locFVec)](https://vscode.dev/github/arcowie/ablate/blob/feature-gpu/src/finiteVolume/finiteVolumeSolver.cpp#L234)
		- Calls the function that calculates the residual
	- [x] Create command line option to check if the GPU version should be called ✅ 2024-11-04
		- [-gpu_flow ](https://github.com/arcowie/ablate/blob/193f5b81091bf59dba5ba688ec89089963b5ddac/src/finiteVolume/finiteVolumeSolver.cpp#L241) #commandLineOptionGPU
3. [x] Find the file and function that the residual is calculated in. ✅ 2024-11-04
	- File [faceInterpolant.cpp](https://github.com/arcowie/ablate/blob/946c69604bb87b3750e7dfcb7f2e8931957e8f94/src/finiteVolume/faceInterpolant.cpp) is the file that contains the residual calculation.
	-  **Function** [ablate::finiteVolume::FaceInterpolant::ComputeRHS ](https://github.com/arcowie/ablate/blob/193f5b81091bf59dba5ba688ec89089963b5ddac/src/finiteVolume/faceInterpolant.cpp#L262) in *faceInterpolant.cpp* computes the residual.
4. [x] Create two new files in my fork: ✅ 2024-11-04
		- [faceInteroplantGPU.cpp](https://github.com/arcowie/ablate/blob/dcff1536293a606c5b76d16a29568deb6e62a802/src/finiteVolume/faceInterpolantGPU.cpp)
		- [faceInterpolantGPU.hpp](https://github.com/arcowie/ablate/blob/dcff1536293a606c5b76d16a29568deb6e62a802/src/finiteVolume/faceInterpolantGPU.cpp)
5. [x] Find the file and function that has the Equation of State 🔺 ➕ 2024-11-04 ✅ 2024-11-06
		- **File** [compressibleFlowState.cpp]([https://github.com/arcowie/ablate/blob/193f5b81091bf59dba5ba688ec89089963b5ddac/src/finiteVolume/fieldFunctions/compressibleFlowState.cpp#L6](https://github.com/arcowie/ablate/blob/193f5b81091bf59dba5ba688ec89089963b5ddac/src/finiteVolume/fieldFunctions/compressibleFlowState.cpp#L6)) is the that takes in the function pointers.
		- **File** [mathFunction.hpp]([https://github.com/arcowie/ablate/blob/175fb65dcebef2bb11642ca5fc6b5afbd1f71be1/src/mathFunctions/mathFunction.hpp](https://github.com/arcowie/ablate/blob/175fb65dcebef2bb11642ca5fc6b5afbd1f71be1/src/mathFunctions/mathFunction.hpp)) and [fieldFunction.cpp](https://github.com/arcowie/ablate/blob/175fb65dcebef2bb11642ca5fc6b5afbd1f71be1/src/mathFunctions/fieldFunction.cpp) contain functions for setting up values and storing information about fields.
6. [/] Expand out to call the actual function and inline them 🔺 ➕ 2024-11-06 ✅ 2024-11-13
7. [ ] 