* [[https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#initialization][Cuda programming guide]]
* GPU vs CPU only apps
** With GPU accelerated app
*** Two devices:
**** CPU
**** GPU
*** have to use /cudaMallocManaged()/ to allocate memory on the GPU
*** Data is migrated to GPU
*** Work on GPU is asynchronous to work being performed on CPU
* .cu is the file extension for CUDA-accelrated programs
* CUDA kernal must start with \__global\__ example: 
** 
#+begin_src c
  __global__ void GPUFunction()
  {
    prtinf("This function will run on the GPU.\n");
  }    
#+end_src
** Retrun type for global function MUST be void
** Function marked global can be invoked anywhere CPU or GPU
* Note on terminology
** Host code: typically refers to code run on CPU
** Device code: refers to code run on GPU
** Kernel: refers to the function that will run on the GPU
** Thread: is a logical execution sequence
** Block: is a gropu of threads
** Grid: is a group of blocks
* How to Launch kernal:
** call function with execution configuration
** execution configuration is the code between <<<...>>>  first number defines number of blocks or thread groups, second how many threads to execute in each block
#+begin_src c
  GPUFunction1<<<1, 1>>>();//1 block with 1 thread code will run once
  GPUFunction10<<<10, 1>>>();//10 blocks with 1 thread code will run 10 times
  GPUFunction10<<<1, 10>>>();//1 block with 10 threads code will run 10 times
  GPUFUnction100<<<10, 10>>>();//10 blocks with 10 threads code will run 100 times
#+end_src
** Would launch GPUFunction with 1 thead black and 1 thread in the block
** Kernel code is executed on every thread in every block configured when the kernel is launched
* CPU and GPU are asynchronus, use /cudaDeviceSynchronize()/ to cause CPU execuation to halt until GPU code completes, ie create a synchronization barrier.
* Cuda Provided Thread hierarchy variables
** /gridDim.x/ is the number of blocks in the grid(You can have multiple dimensions, and those are accessed using /gridDim.y/ etc..)
** /blockIdx.x/ is the index of the current block within the grid.  Uses 0 based indexing so first index is 0.
** /blockDim.x/ is the number of threads in the given block(All blocks in a gird have the same number of threads)
** /threadIdx.x/ is the index of the thread within a block also 0 based indexing so first thread in a block is 0
*** Also the index resets for each block, ie the first thread in every block had id 0.
* Dynamic memmory allocation
** /cudaMallocManaged(**<name of pointer>, <size in bytes>)/
*** The pointer passed to /cudaMallocManaged/ is accessable by both host and device code.
** /cudaFree(<name of pointer>)/ to free the memory
* [[https://docs.nvidia.com/cuda/cuda-c-best-practices-guide/index.html#memory-optimizations][CUDA Best Practices Guide]] recommends *APOD* Assess, Parallelize, Optmize, Deploy
* NSYS: cuda, profiling utility
** To use the cmd line looks like nsys profile --stats=true ./[application name]
* Streaming multiprocessors(SM):
** More than 1 block can be scheduled on an SM, however a single block will never be split over multiple SMs
** Ideal to create grid dimensions that are evenly divisable by the number of SMs for best utilization.
** Also SMs group threads into units called warps, which are 32 threads.  Try and choos number of threads in a block to be a multiple of 32!
** Should never hard code number of SMs.  To make code portable you can query the GPU to get its properties
** [[https://docs.nvidia.com/cuda/cuda-runtime-api/structcudaDeviceProp.html][Cuda device properties]]
#+begin_src c
  int deviceId;
  cudaGetDevice(&deviceId); //deviceId now has the id of the currently active GPUfunction

  cudaDeviceProp prop; //Struct that has the fields to hold all the properties of the GPU
  cudaGetDeviceProperties(&prop, deviceId); //props now has many of the properties of the GPU
#+end_src
* Unified Memory Behavior:  ie when using cudaMallocManaged
** When first allocated it may not be on the CPU or GPU
** When some memory is accessed first time will trigger a [[https://en.wikipedia.org/wiki/Page_fault][page fault]].
*** This triggers memory migration to the requesting device, CPU or GPU
*** This will happen back and forth,
**** If memory initialized on CPU, page fault will cause it to be allocated on CPU
**** Next when kernel launched and eventually accessed there will be a page fault on the GPU and the memory will be copied.
**** When control returns to the CPU if it tried to access the data another page fault will be triggered and the memory will be copied to the CPU.
*** can do async prefetching to a location using cudaMemPrefetchAsync([pointerToSomeData], [size], [location])
*** 
#+begin_src c
  int deviceId;
  cudaGetDevice(&deviceId);                                         // The ID of the currently active GPU device.

  cudaMemPrefetchAsync(pointerToSomeUMData, size, deviceId);        // Prefetch to GPU device.
  cudaMemPrefetchAsync(pointerToSomeUMData, size, cudaCpuDeviceId); // Prefetch to host. `cudaCpuDeviceId` is a
                                                                    // built-in CUDA variable.
#+end_src
* Streams:
** There are two basic types of streams.  The default stream and the non-default streams.
** In all streams each kernel in that stream must run in serial fashion, and in the /order issued/.
** However, kernels in non-default streams may run concurently with regards to each other. 
** The /default stream is special/, it will block kernels in all other streams from running.
** Creating, Utilizing, and Destorying non-default streams 
*** 
#+begin_src c
  cudaStream_t stream;       // CUDA streams are of type `cudaStream_t`.
  cudaStreamCreate(&stream); // Note that a pointer must be passed to `cudaCreateStream`.

  someKernel<<<number_of_blocks, threads_per_block, 0, stream>>>(); // `stream` is passed as 4th EC argument.

  cudaStreamDestroy(stream); // Note that a value, not a pointer, is passed to `cudaDestroyStream`.
#+end_src
** Many Cuda runtime functions expect a stream argument, they /all/ have a default value of 0, default stream.
** Kernel launches /always/ take palce in streams.
* Optiiazation strategy Copy/Compute Overlap:
** Three basic steps to GPU computing:
*** Copy/trasnfer data to GPU.
*** Do Computation.
*** Copy data back to host.
*** Total run time would be the sum of those three steps.
** Overlap copy data to GPU and compute and overlap Copy back to host.
*** So total run time is no longer the sum of the three but less based on the amount of the overlap.
*** 
* [[https://docs.nvidia.com/nsight-systems/index.html][Nsight]]:
** Cuda profiling application
** To run 
#+begin_src bash
  nsys profile -o [report name] ./[program to run]
#+end_src
** To evaluate cuda kernels use [[https://developer.nvidia.com/nsight-compute][Nsight Computer]]
