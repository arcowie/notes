## How to get PETSc to use your local copy of an external library that you have made changes to.
	--download-[external package name]-commit=HEAD
## Potential edits to build system
- check_for_option_mistakes.py, [check_for_option_mistakes(opt)](https://github.com/petsc/petsc/blob/2355682a5863f6075d0c727c85fd96682baa0e2a/config/configure.py#L33)
- 
	```python
		for exc in ['mkl_sparse', 'mkl_sparse_optimize', 'mkl_cpardiso', 'mkl_pardiso', 'superlu_dist', 'PETSC_ARCH', 'PETSC_DIR', 'CXX_CXXFLAGS', 'LD_SHARED', 'CC_LINKER_FLAGS', 'CXX_LINKER_FLAGS', 'FC_LINKER_FLAGS', 'AR_FLAGS', 'C_VERSION', 'CXX_VERSION', 'FC_VERSION', 'size_t', 'MPI_Comm','MPI_Fint','int64_t']:
        if name.find(exc) >= 0:
          exception = True
	```
- Change to
```python
if name in exc:
	exception = True	
```
- [ ] Changed code in check_for_options_mistakes(opts) ➕ 2024-11-20
	- Do you need this [code](https://github.com/petsc/petsc/blob/2355682a5863f6075d0c727c85fd96682baa0e2a/config/configure.py#L42) given the above code
```python
for exc in ['mkl_sparse', 'mkl_sparse_optimize', 'mkl_cpardiso', 'mkl_pardiso', 'superlu_dist']:

if name.find(exc.replace('_','-')) > -1:

raise ValueError('The option '+opt+' should be '+opt.replace(exc.replace('_','-'),exc));
```
- [ ] removing code.  Also ask about semicolons ➕ 2024-11-20
-  add comment about deprecated [ifneeded](https://github.com/petsc/petsc/blob/2355682a5863f6075d0c727c85fd96682baa0e2a/config/configure.py#L40) following PETSc 3.2 should be --download-[package] and change message.
- [ ] added comment and changed message ➕ 2024-11-20
- configure.py,
	- [check_for_option_changed(opts)](https://github.com/petsc/petsc/blob/2355682a5863f6075d0c727c85fd96682baa0e2a/config/configure.py#L51)
	- [check_for_unsupported_combinations(opts)](https://github.com/petsc/petsc/blob/2355682a5863f6075d0c727c85fd96682baa0e2a/config/configure.py#L47)
- [ ] Change to proper option automatically and make a note in the log or prompt user ➕ 2024-11-20
