[Link to PETSc Manual](https://petsc.org/release/manual/dmplex/)
# Overview:
	DMPlex is a subclass of DM for handling unstructured grids.
	
## Tasks
- [/] Finish DMPlex Notes ⛔ doq4gm ➕ 2024-11-04
- [ ] Add graphs 🆔 🆔 doq4gm ⛔ cui6mx,0ad5sm,qp8cfs ➕ 2024-11-04
- [ ] Create Cone Graph 🆔 cui6mx ➕ 2024-11-04
- [ ] Create Support graph 🆔 0ad5sm ➕ 2024-11-04
- [ ] Create Transitive closure graph 🆔 qp8cfs ➕ 2024-11-04