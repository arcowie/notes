
### Running tests:
1. Make sure PETSC_DIR and PETSC_ARCH are set
2. Go into top level PETSc dir
3. make -f ./gmakefile test search="dir_to_test-testName_test_suffix" EXTRA_OPTIONS="/-what ever/"



### Rebuilding PETSc
1. Make sure PETSC_DIR and PETSC_ARCH are set
2. go to PETSc root directory
3. Run make -f ./gmakefile test search="dm_impls_plex_test-ex5_0"
4. If that doesn't work
   1. $PETSC_DIR/PETSC_ARCH/lib/petsc/conf/reconfigure-[name]-[of]-[arch].py
   2. run script petscReconf.py
      



```python
  import os
  def getPetscDir():
      petscDir = os.environ.get('PETSC_DIR')
      if petscDir is None:
  	  print(f'\nThe enviromental variable PETSC_DIR can not be found\n'
                  'if you wish to continue type n, else input the absolute path where' 
                  'PETSc is installed\n')
      else:
          return petscDir
      value = input()
      if value.lower() == 'n':
  	print(f'\nYou entered {value}, exiting script\n')
  	exit()
      else:
  	counter = 0
  	while counter == 0:
  	    print(f'\nYou entered {value}\nIf this is correct type y or type n to reinput path\n')
  	    value =input ()
  	    if value.lower() == 'y':
  		return value
  	    else:
  		print(f'\nPlease enter the new path\n')
  		value = input()
  def getArches(arches, petscDir):
      """
      This function takes two parameters, arches and petscDir and returns a list of valid arches as a list of strings
      The default behavior is when the values of arches is None.  Then the funciton will use os.listdir in the director
      that was provided in petscDir and return a list with all directories that begin with arch.

      If arches is not None, then it will compare the list of arches with the results from os.listdir and return a list
      of all arch names that are valid, ie that were found using os.listdir.  The function will output which arch names
      were not found.  If no valid arches were found it will exit.

      Parameters:
      arches (str): Default is None, otherwise a list of arch names as strings.
      petscDir (str): Contains a string with the absolute path to PETSc.

      Returns:
      str: List of valid arches
      """
      archListTemp = []
      archListSub = []
      for entry in os.listdir(petscDir):
  	if 'arch' in entry:
  	    archListTemp.append(entry)
      if arches == None:
  	if len(archListTemp)==0:
  	    print(f'No valid arches were found in path: {petscDir}\n'
                    'This could be because you have not created any using ./configure or'
                    ' Your arches do not start with arch.  Please check your path.\n'
                    'if your arches do not start with arch you will have to input them'
                    'individually using the command line argument -a or --arches\n'
                    'exiting')
  	    exit()
  	else:
  	    return archListTemp
      else:
  	for item in arches:
  	    if item in archListTemp:
                  archListSub.append(item)
  	    else:
  	        print(f'Specified arch: [{item}] not found.\n')
  	if len(archListSub) == 0:
  	    print(f'None of the specified arches: {arches} found.\nExiting')
  	    exit()
  	else:
  	    return archListSub  

  petscDir=getPetscDir()
  archList=getArches(None, petscDir)
  print("hello world")
  for arch in archList:
      print(arch)

```


### Makefile example
```make
include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

target: source.o
	${CLINKER} -o $@ $^ ${PETSC_LIB}

```
























































































































































































































































































































































** 
