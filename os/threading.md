* Threading
** *NOTE* when compiling a program that uses threads -pthread must be included as a flag.
** Thread creation
#+begin_src c
  #include <pthread.h>
  int pthread_create(pthread_t *thread,
                     const pthread_att_t *attr,
                     void *(*start_routine)(void*),
                     void *arg)
#+end_src
*** *pthread_t*: pointer to the thread to create
*** *const pthread_att_t*: attributes of the thread, can be NULL
*** **start_routine*: function pointer, that points to where the threat willl start.
*** **args*: arguments passed to the function that the thread starts executing.
** Use pthread_join() to force a thread to wait for antoher to complete.
#+begin_src c
  int pthread_join(pthread_t thread, void **value_ptr)
#+end_src
*** The thread executing this line must wait for *thread* to finish before continuing execution.
*** *value_ptr* is passed a copy of the exit status of the thread, if supplied, i.e. the value of the return statement.
*** returns 0 if thread exists normally, otherwise it returns an error code.
** /NOTE/ Never return a pointer to a value allocated on threads stack!!
** The information in the following sections comes from [[https://pages.cs.wisc.edu/~remzi/OSTEP/][OSTEP]]
* Critical sections
** Sections of code where processes access a shared resource such as a global varaible, file, etc and perfrom a write
** How to protect critical sections
*** Locks
**** Called a /mutex/ in pthread POSIX lib for mutual exclusion.
#+begin_src c
  pthread_mutex_t lock;
  pthread_mutex_init(&lock, NULL);
  int rc = pthread_mutex_lock(&lock);
  x=x+1; // critical code block
  pthread_mutex_unlock(&lock);
#+end_src
**** Only the thread with the lock can proceed, all other threads will wait until pthread_mutex_unlock is called.
**** /Always/ need to initialize a lock before using it.
**** call pthread_mutex_destroy() when done with lock.
**** Should always check the return code of the lock and unlock to ensure that it succeeded.
**** There are other routines in the pthread's libraries that work with locks.
**** Can use multiple lock variables for more fine grained control.
**** /How do locks work/
***** Hardware support called test and set, which is an atomic instruction, looks like:
***** Spin Lock
#+begin_src c
  int TestAndSet(int *old_ptr, int new){
    int old = *old_ptr; //get the old value from old_ptr
    ,*old_ptr=new;  // store the "new" value in old_ptr (this is the set part)
    return old;  // return the old value (this is the test part)
  }
#+end_src
***** /NOTE/ to work correctly this requires a /preemptive scheduler/ on a single CPU system otherwise threads would /never/ need to worry.
***** Compare-and-swap or compare-and-exchange
#+begin_src  c
  int CompareAndSwap(int *ptr, int expected, int new){
    int original = *ptr;
    if(original == expected) *ptr = new;
    return orginal;
  }
#+end_src
*** Condition variables
**** /Definition/: A /condition variable/ is an explicit queue that threads can put themselves on when some state of execution(some condition) is not as desired (by waiting on the condition); some other thread, when it changes said state, can then wake one (or more)of those waiting threads and thus allow them to continue (by /signaling/) on the condition).
**** Useful for when signaling has to take place between threads.
**** Two primray routines
***** int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex)
***** int pthread_cond_signal(pthread_cond_t *cond)
**** In addition to a lock that is associated with the condition another lock should also be held. 
**** 
#+begin_src c
  pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

  pthread_mutex_lock(&lock);
  while (ready==0)
    pthread_cond_wait(&cond, &lock);
  pthread_mutex_unlock(&lock);

  //code used by another thread to wake the first
  pthread_mutex_lock(&lock);
  ready=1;
  pthread_cond_signal(&cond);
  pthread_mutex_unlock(&lock);
#+end_src
**** *NOTES*
***** When signinaling as well as modifying the gobal varaible ensure the lock is held
***** pthread_cond_wait takes lock as well because it releases it while the thread sleeps.
***** Before returning and after being woken up it reaquires the lock
***** /Mesa Semantics/ are used by almost all libraries.  This means when a thread is signaled it is set to a ready state for the scheduler, but another thread may run first.  Therefore /always/ use a while loop around the wait.
*** Semaphores
**** /Definition/: An object with an interger value that we can manipulate with two routines; POSIX standard
***** sem_wait()
#+begin_src c
  int sem_wait(sem_t *s){
    decrement the value of semaphore s by one wait if value of semaphore is negative
  }
#+end_src
***** sem_post()
#+begin_src c
  int sem_post(sem_t *s){
      increment the value of semaphore s by one, if there are one or more threads waiting, wake one
    }
#+end_src
**** /Note/ always initialize a semaphore as behavior is based on the integer value
#+begin_src c
  #include <semaphore.h>
  sem_t s;
  sem_init(&s, 0, 1);
#+end_src
**** In sem_init(&s, 0, 1); the 0 means it is shared by all threads in the same process and the 1 is the initial value.  See the [[man:sem_init][man page]] for shareing between processes and other options.
**** What to /initialize/ the semaphore to?!?
***** Think of how many resources to give away....
***** In the case of the lock or Binary Semaphore, you give a way 1 resource, the lock, so initialize to 1.
***** In the case of the ordering problem we set it to 0 because there is no initial resource to give out, only after the child goes is the resource created!
**** /Binary Semaphores(Locks)/
***** initialize semaphore to 1
***** Surround critical section
****** sem_wait()
****** /critcal section/
****** sem_post()
**** ------------------------------------------------------------------
| Val | Thread 0             | State | Thread 1           | State |
|-----+----------------------+-------+--------------------+-------|
|   1 |                      | Run   |                    | Ready |
|   1 | Call sem_wait()      | Run   |                    | Ready |
|   0 | sem_wait() /returns/   | Run   |                    | Ready |
|   0 | /crit section begins/  | Run   |                    | Ready |
|   0 | /Interrupt;Switch->T1/ | Ready |                    | Run   |
|   0 |                      | Ready | Call sem_wait()    | Run   |
|  -1 |                      | Ready | /(sem<0)->sleep/     | Sleep |
|  -1 |                      | Ready | /Switch->T0/         | Sleep |
|  -1 | /crit sect ends/       | Run   |                    | Sleep |
|  -1 | Call sem_post()      | Run   |                    | Sleep |
|   0 | incr sem             | Run   |                    | Sleep |
|   0 | wake(T1)             | Run   |                    | Ready |
|   0 | sem_post() /returns/   | Run   |                    | Ready |
|   0 | /Interrupt;Switch->T1/ | Ready |                    | Run   |
|   0 |                      | Ready | sem_wait() /returns/ | Run   |
|   0 |                      | Ready | /crit sect/          | Run   |
|   0 |                      | Ready | call sem_post()    | Run   |
|   1 |                      | Ready | sem_post() /retruns/ |       |
**** /Semaphores For Ordering/
#+begin_src c
  sem_t s;

  void *child(void *arg){
    printf("child\n");
    sem_post(&s); //signal child is done
    return NULL;
  }

  int main(int argc, *argv[]){
    semi_init(&s, 0 , 0); //needs to be 0 incase parent goes first
    printf("parent: begin\n");
    pthread_t c;
    pthread_create(&c, NULL, child, NULL);
    sem_wait(&s); //wait for child
    prtinf("parent: end\n");
    return 0;
  }
#+end_src
***** Need to initialize semaphore to 0 in this case in child parent goes first, this will cause s to be decramented to -1 and parent thread to wait.
**** /The Producer/Consumer(Bound Buffer) Problem/
#+begin_src c
  sem_t empty;
  sem_t full;

  int buffer[MAX];
  int fill = 0;
  int use = 0;

  void put(int value) {
   buffer[fill] = value; // Line F1
   fill = (fill + 1) % MAX; // Line F2
   }

  int get() {
   int tmp = buffer[use]; // Line G1
   use = (use + 1) % MAX; // Line G2
   return tmp;
   }

  void *producer(void *arg) {
   int i;
   for (i = 0; i < loops; i++) {
   sem_wait(&empty); // Line P1
   sem_wait(&mutex); // Line P1.5 (lock)
   put(i); // Line P2
   sem_post(&mutex); // Line P2.5 (unlock)
   sem_post(&full); // Line P3
   }
  }

   void *consumer(void *arg) {
   int i;
   for (i = 0; i < loops; i++) {
   sem_wait(&full); // Line C1
   sem_wait(&mutex); // Line C1.5 (lock)
   int tmp = get(); // Line C2
   sem_post(&mutex); // Line C2.5 (unlock)
   sem_post(&empty); // Line C3
   printf("%d\n", tmp);
   }
  }

   int main(int argc, char *argv[]) {
   // ...
   sem_init(&empty, 0, MAX); // MAX are empty
   sem_init(&full, 0, 0); // 0 are full
   // ...
  }
#+end_src
***** The semaphore full and empty need to be outside the /binarty semaphore/ or lock to prevent deadlock
***** This will work for multiple producer and consumer threads.
**** Reader-Writer Locks
***** Need a more flexible lovking primitive.
***** Consider a list, multiple concurent reads are fine on a data structure as long as no inserts are going on.
***** What is needed is called a /reader-writer lock/
#+begin_src c
  typedef struct _rwlock_t {
   sem_t lock; // binary semaphore (basic lock)
   sem_t writelock; // allow ONE writer/MANY readers
   int readers; // #readers in critical section
  } rwlock_t;

  void rwlock_init(rwlock_t *rw) {
   rw->readers = 0;
   sem_init(&rw->lock, 0, 1);
   sem_init(&rw->writelock, 0, 1);
  }

  void rwlock_acquire_readlock(rwlock_t *rw) {
   sem_wait(&rw->lock);
   rw->readers++;
   if (rw->readers == 1) // first reader gets writelock
   sem_wait(&rw->writelock);
   sem_post(&rw->lock);
  }

  void rwlock_release_readlock(rwlock_t *rw) {
   sem_wait(&rw->lock);
   rw->readers--;
   if (rw->readers == 0) // last reader lets it go
   sem_post(&rw->writelock);
   sem_post(&rw->lock);
  }

  void rwlock_acquire_writelock(rwlock_t *rw) {
   sem_wait(&rw->writelock);
  }

  void rwlock_release_writelock(rwlock_t *rw) {
   sem_post(&rw->writelock);
  }
#+end_src
***** While this works, it does have the draw back of lacking /fairness/, where the consumer starves.  Also can be done with less overhead using simple locks.
**** Famous problem, /The Dining Philosophers/:
***** /Problem:/ Assume there are five “philosophers” sitting around a table. Between eachpair of philosophers is a single fork (and thus, five total). The philosophers each have times where they think, and don’t need any forks, and times where they eat. In order to eat, a philosopher needs two forks, both the one on their left and the one on their right. The contention for these forks, and the synchronization problems that ensue, are what makes thisa problem we study in concurrent programming.
***** /Solution:/ Have four philosophers initiat locks and grab the right fork first, and one grab the left one first.  This will ensure no deadlock!
****** Solved by Dijkstra!
**** Thread Throttling:
***** One other simple use case for semaphores arises on occasion, and thus we present it here. The specific problem is this: how can a programmer prevent “too many” threads from doing something at once and bogging the system down? Answer: decide upon a threshold for “too many”, and then use a semaphore to limit the number of threads concurrently executing the piece of code in question. We call this approach throttling [T99], and consider it a form of admission control. Let’s consider a more specific example. Imagine that you create hundreds of threads to work on some problem in parallel. However, in a certain part of the code, each thread acquires a large amount of memory to perform part of the computation; let’s call this part of the code the memory-intensive region. If all of the threads enter the memory-intensive region at the same time, the sum of all the memory allocation requests will exceed the amount of physical memory on the machine. As a result, the machine will start thrashing (i.e., swapping pages to and from the disk), and the entire computation will slow to a crawl. A simple semaphore can solve this problem. By initializing the value of the semaphore to the maximum number of threads you wish to enter the memory-intensive region at once, and then putting a sem wait() and sem post() around the region, a semaphore can naturally throttle the number of threads that are ever concurrently in the dangerous region of the code.
