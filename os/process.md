* Process: is a Running program.
** Machine State of the process:
*** Memory:  The memory that the process can access is called the /address space/ 
**** This contains the instructions and as well as datat being read and written.
*** Registers:  Many instructions explicitly read or update data in registers so these are part of the state.
**** Some Special registers:
***** [[https://en.wikipedia.org/wiki/Program_counter][Program Counter(PC)]]: also called instruction pointers, points to the memory location of the next instruction.
***** [[http://www.ee.nmt.edu/~erives/308L_05/The_stack.pdf][Stack Pointer]] and [[https://people.cs.rutgers.edu/~pxk/419/notes/frames.html#:~:text=Understanding%20Frame%20Pointers&text=A%20frame%20pointer%20(the%20ebp,offsets%20to%20the%20frame%20pointer.][Frame Pointer]]: are used to manadge the stack for function parameters, local variables and return addresses.
*** I/O: programs often access files so a list of open files is also kept, if they exist.
* Process Creation:
** The OS creates an /address space/:
*** /code/ and /static data/ from the disk are loaded into the /address space/ from a hard disk or SSD.
*** The /heap/ and /stack/ spaces are created in the /address space/.
**** Stack is initialized with /argc/ and /argv/ array from main in a C program.
***** Note remember the stack starts from the top or largest address and down to smaller address i.e. negative direction.  So you subtract from the frame pointer address (which does not change during the execution of a function) to get the new address of the stack pointer as room is made for variables.
**** [[https://www.geeksforgeeks.org/what-is-a-memory-heap/][Heap]] is initially small butr grows with calls from functions like [[man:malloc][malloc()]] or [[man:calloc][calloc()]]
***** The heap starts above the /text/, /data/, /bss/, segements and grows to larger address, i.e. in a positive direction.
**** For an image of a typical proccess layout in memory see this [[https://www.researchgate.net/figure/1-UNIX-Process-Memory-Layout_fig7_286921747][link]]
** The /code/ and /static data/ are in some kind of /executable format/.
** Early OSes used eager loading, i.e. everything was loaded at once.
** Current OSes use /lazy/ loading, where only pieces of code and data that are needed are loaded.
*** TODO [Add links] See paging and swapping for details
** /UNIX/ standard is to have three open /file descriptors/
*** /input/
*** /output/
*** /error/
* Process States
** There are three states:
*** /Running/: Process is running on the processor/executing instructions.
*** /Ready/: Process is ready to run but the OS has chosen to not run it at the given time.
*** /Blocked/: Process is performing a an operation that prevents it from running until complete.  An example would be an I/O request.  A process will remain blocked until the blocking operation completes
** /Scheduled/: Is the term for moving a process from /Ready/ to /Running/. 
** /Descheduled/: Is the term for moving a process from /Running/ to /Ready/ or /Blocked/.
* Data Structure:
** The OS has to track the state of each process and therefore will keep some kind of /process list/.  An example of what the data for each process might look like;
#+begin_src c
  // the registers xv6 will save and restore
  // to stop and subsequently restart a process
  struct context {
   int eip;
   int esp;
   int ebx;
   int ecx;
   int edx;
   int esi;
   int edi;
   int ebp;
  };
  // the different states a process can be in
  enum proc_state { UNUSED, EMBRYO, SLEEPING,
                    RUNNABLE, RUNNING, ZOMBIE };
  // the information xv6 tracks about each process
  // including its register context and state
  struct proc {
   char *mem; // Start of process memory
   uint sz; // Size of process memory
   char *kstack; // Bottom of kernel stack for this process
   enum proc_state state; // Process state
   int pid; // Process ID
   struct proc *parent; // Parent process
   void *chan; // If !zero, sleeping on chan
   int killed; // If !zero, has been killed
   struct file *ofile[NOFILE]; // Open files
   struct inode *cwd; // Current directory
   struct context context; // Switch here to run process
   struct trapframe *tf; // Trap frame for the  current interrupt
  };
#+end_src
** A struct like this and the process is sometimes called a [[https://en.wikipedia.org/wiki/Process_control_block][Process Control Block]]
*** TODO [add link to context switch] This is information is needed for /context switch/, ie switching between processes 
* Process API:
** [[man:fork][fork()]]: System call used to ceate a new process.
*** Creates an almost exact copy of the calling program.
*** The new program has its own PC, PCB, memory, registers, etc.
*** The new program and the old program continue to execute code starting with the instruction after fork()
*** /NOTE/ the return value is the difference.
**** In the parent instance the return value of fork() is the PID of the new child process or -1 if failed.
**** In the child instance the return value is 0. 
** [[man:wait][wait()]]: System call that causes the parent to wait/block until the child process finishes.
*** wait() return value is the child's PID.
*** Makes the code it contains deterministic.  When fork() is called there is no way to know if the child or parent will execute first.
** [[man:waitpid][waitpid()]]: Default behavior is to the same was wait, however can pass other values to wait for a process with specific PID to finish.  See man page.
** [[man:exec][exec()]]: System call that will run a different program, note there are six varients; [[man:execl][execl()]], [[man:execlp][execlp()]], [[man:execle][execle()]], [[execv][execv()]], [[execvp][execvp()]], [[execvpe][execvpe()]].
*** The base varient will cause the current process's code segment to be overwritten by what ever program is called by exec, then the OS runs that program.  Instead of creating a new process it transforms the original process.
*** /NOTE/ a sucessful call to exec() does not return anything.
