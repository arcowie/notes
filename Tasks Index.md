# High priority Tasks
```tasks
priority is above medium
```

# Tasks Due Today
```tasks
due on today
```

# All tasks sorted by priority
```tasks
not done
hide created date
sort by priority
```

